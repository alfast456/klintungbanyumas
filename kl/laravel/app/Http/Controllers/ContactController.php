<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    public function simpan()
    {
        $data = new Contact();
        $data->nama=request('nama');
        $data->email=request('email');
        $data->pesan=request('pesan');
        $data->save();
        return redirect()->back();
    }
}
