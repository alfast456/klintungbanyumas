<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;

use App\datauser;
use Illuminate\Http\Request;

class DatauserController extends Controller
{
    public function index()
    {
        $users = Datauser::all();
        return view('dashboard.user', ['datausers' => $users]);
    }

    public function show($datauser_id)
    {
        $result = Datauser::findOrFail($datauser_id);
        return view('datauser.show', ['datauser' => $result]);
    }

    public function create()
    {
        return view('datauser.create');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $hashed = Hash::make('password'); // encrypt the password
        $user = new Datauser();
        $user->nama = $validateData['nama'];
        $user->no_hp = $validateData['no_hp'];
        $user->email = $validateData['email'];
        $user->password = $hashed;
        $user->save();
        $request->session()->flash('pesan', 'Penambahan data berhasil');
        return redirect()->route('datauser.index');
    }

    public function edit($datauser_id)
    {
        $result = Datauser::findOrFail($datauser_id);
        return view('datauser.edit', ['datauser' => $result]);
    }

    public function update(Request $request, Datauser $datauser)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $hashed = Hash::make('password'); // encrypt the password
        $datauser->nama = $validateData['nama'];
        $datauser->email = $validateData['email'];
        $datauser->no_hp = $validateData['no_hp'];
        $datauser->password = $hashed;
        $datauser->save();
        $request->session()->flash('pesan', 'Perubahan data berhasil');
        return redirect()->route('dashboard.user', ['datauser' => $datauser->id]);
    }

    public function destroy(Request $request, Datauser $datauser)
    {
        $datauser->delete();
        $request->session()->flash('pesan', 'Hapus data berhasil');
        return redirect()->route('dashboard.user');
    }
}
