<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Destinasi;
class BookingController extends Controller
{
    public function simpan(Request $request)
    {
        $destinasi = Destinasi::find(request('destinasi'));
        $data = new Booking();
        $data->destinasi=request('destinasi');
        $data->email_user=request('email_user');
        $data->no_hp=request('no_hp');
        $data->tgl_booking=request('tgl_booking');
        $data->jumlah_tiket=request('jumlah_tiket');
        $data->total_harga=request('jumlah_tiket')*$destinasi->harga;
        $data->save();
        $request->session()->flash('pesan','Pesanan anda berhasil, silahkan cek email anda!');
        return redirect()->back();
    }
}
