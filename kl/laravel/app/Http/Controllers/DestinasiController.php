<?php

namespace App\Http\Controllers;

use App\Destinasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DestinasiController extends Controller
{
    public function hitung()
    {
        $wisatas = Destinasi::count();
        return view('dashboard.dashboard', compact('wisatas'));
    }
    public function klintung()
    {
        $wisatas = Destinasi::all();
        return view('klintung.index', ['destinasis' => $wisatas]);
    }

    public function detail($destinasi_id)
    {
        $result = Destinasi::findOrFail($destinasi_id);
        return view('klintung.detailv1', ['destinasi' => $result]);
    }

    public function explore()
    {
        $result = Destinasi::all();
        return view('klintung.listingv1', ['destinasi' => $result ] );
    }

    public function exploresearch()
    {
        $result = Destinasi::where('nama', request('nama'))
        ->orWhere('nama', 'like', '%' . request('nama') . '%')->get();

        return view('klintung.listingv1', ['destinasi' => $result ] );
    }

    public function index()
    {
        $wisatas = Destinasi::all();
        return view('dashboard.datawisata', ['destinasis' => $wisatas]);
    }

    public function show($destinasi_id)
    {
        $result = Destinasi::findOrFail($destinasi_id);
        return view('dashboard.datawisata', ['destinasi' => $result]);
    }

    public function create()
    {
        return view('destinasi.create');
    }

    public function store(Request $request)
    {

        $validateData = $request->validate([
            'nama' => 'required',
            'lokasi' => 'required',
            'maps' => 'required',
            'nohp_wisata' => 'required',
            'video' => 'required',
            'jam' => 'required',
            'jam2' => 'required',
            'jam3' => 'required',
            'jam4' => 'required',
            'jam5' => 'required',
            'jam6' => 'required',
            'jam7' => 'required',
            'tutup' => 'required',
            'deskripsi' => 'required',
            'foto' => 'mimes:jpg,jpeg,JPEG,png,gif,bmp', 'max:2024',
            'harga' => 'required',
        ]);
        $imgName = $request->foto->getClientOriginalName() . '-' . time()
                                . '.' . $request->foto->extension();
        $destination = base_path() . '/public/download';
        $request->foto->move($destination, $imgName);

        $wisata = new Destinasi();
        $wisata->nama = $validateData['nama'];
        $wisata->lokasi = $validateData['lokasi'];
        $wisata->maps = $validateData['maps'];
        $wisata->nohp_wisata = $validateData['nohp_wisata'];
        $wisata->video = $validateData['video'];
        $wisata->jam = $validateData['jam'];
        $wisata->jam2 = $validateData['jam2'];
        $wisata->jam3 = $validateData['jam3'];
        $wisata->jam4 = $validateData['jam4'];
        $wisata->jam5 = $validateData['jam5'];
        $wisata->jam6 = $validateData['jam6'];
        $wisata->jam7 = $validateData['jam7'];
        $wisata->tutup = $validateData['tutup'];
        $wisata->deskripsi = $validateData['deskripsi'];
        $wisata->foto = $imgName;
        $wisata->harga = $validateData['harga'];
        $wisata->save();
        $request->session()->flash('pesan', 'Penambahan data berhasil');
        return redirect()->route('dashboard.datawisata');
    }

    public function edit($destinasi_id)
    {
        $result = Destinasi::findOrFail($destinasi_id);
        return view('destinasi.edit', ['destinasi' => $result]);
    }

    public function update(Request $request, Destinasi $destinasi)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'lokasi' => 'required',
            'maps' => 'required',
            'nohp_wisata' => 'required',
            'video' => 'required',
            'jam' => 'required',
            'jam2' => 'required',
            'jam3' => 'required',
            'jam4' => 'required',
            'jam5' => 'required',
            'jam6' => 'required',
            'jam7' => 'required',
            'tutup' => 'required',
            'deskripsi' => 'required',
            'foto' => 'mimes:jpg,jpeg,JPEG,png,gif,bmp', 'max:2024',
            'harga' => 'required',
        ]);
        $imgName = $request->foto->getClientOriginalName() . '-' . time()
                                . '.' . $request->foto->extension();
        $destination = base_path() . '/public/download';
        $request->foto->move($destination, $imgName);

        $destinasi->nama = $validateData['nama'];
        $destinasi->lokasi = $validateData['lokasi'];
        $destinasi->maps = $validateData['maps'];
        $destinasi->nohp_wisata = $validateData['nohp_wisata'];
        $destinasi->video = $validateData['video'];
        $destinasi->jam = $validateData['jam'];
        $destinasi->jam2 = $validateData['jam2'];
        $destinasi->jam3 = $validateData['jam3'];
        $destinasi->jam4 = $validateData['jam4'];
        $destinasi->jam5 = $validateData['jam5'];
        $destinasi->jam6 = $validateData['jam6'];
        $destinasi->jam7 = $validateData['jam7'];
        $destinasi->tutup = $validateData['tutup'];
        $destinasi->deskripsi = $validateData['deskripsi'];
        $destinasi->foto = $imgName;
        $destinasi->harga = $validateData['harga'];
        $destinasi->save();
        $request->session()->flash('pesan', 'Perubahan data berhasil');
        return redirect()->route('dashboard.datawisata', ['destinasi' => $destinasi->id]);
    }

    public function destroy(Request $request, Destinasi $destinasi)
    {
        $destinasi->delete();
        $request->session()->flash('pesan', 'Hapus data berhasil');
        return redirect()->route('destinasi.index');
    }
}
