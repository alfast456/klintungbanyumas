<?php
namespace App\Http\Controllers;

use App\Admin;
use App\Datauser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index()
    {
        return view('login.login');
    }
    public function process(Request $request)
    {
        $validateData = $request->validate(['username' => 'required', 'password' => 'required', ]);
        $result = Datauser::where('email', request('username'))->first();
        if ($result)
        {
            if ((Hash::check(request('password'),$result->password)))
            {
                Auth::user($result->id);
                session(['username' => $request->username]);
                return redirect('/home');
                // Auth::loginUsingId($result->id);
                // session(['username' => $request->username]);
                // return redirect('/home');
            }
            else
            {
                return back()
                    ->withInput()
                    ->with('pesan', "Login Gagal");
            }
        }
        else
        {
            return back()
                ->withInput()
                ->with('pesan', "Login Gagal");
        }
    }
    public function logout()
    {
        session()
            ->forget('username');
        return redirect('login')
            ->with('pesan', 'Logout berhasil');
    }
}

