<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destinasi;
use App\Riview;

class RiviewController extends Controller
{
    public function simpan(Request $request)
    {
        $destinasi = Destinasi::find(request('destinasi'));
        $data = new Riview();
        $data->destinasi=request('destinasi');

        if ($request->hasFile('gambar')) {
            $destinationPath = base_path() . '/public/download';
            $files = $request->file('gambar'); // will get all files
            $file_name = $files->getClientOriginalName(); //Get file original name
            $files->move($destinationPath , $file_name); // move files to destination folder
            $data->gambar=$file_name;
        }

        $data->bintang=request('bintang');
        $data->komentar=request('komentar');
        $data->save();
        return redirect()->back();
    }
}
