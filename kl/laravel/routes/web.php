<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('dashboard.dashboard');
});
Route::get('/', function () {
    return view('klintung.index');
});

Route::get('dashboard/booking', function () {
    return view('dashboard.booking');
});
Route::get('dashboard/reviews', function () {
    return view('dashboard.reviews');
});
Route::get('dashboard/myprofile', function () {
    return view('dashboard.myprofile');
});
Route::get('dashboard/addwisata', function () {
    return view('dashboard.addwisata');
});
Route::get('/home', function () {
    return view('klintung.index');
});
Route::get('/booking', function () {
    return view('klintung.booking');
});
Route::post('/booking', 'BookingController@simpan');

Route::get('/explore', 'DestinasiController@explore');
Route::get('/explore/search/', 'DestinasiController@exploresearch');
Route::get('/contact', function () {
    return view('klintung.contactus');
});
Route::post('/contact', 'ContactController@simpan');
Route::get('/profile', function () {
    return view('klintung.profile');
});
Route::get('/awal', function () {
    return view('klintung.index');
});

Route::get('/', 'DestinasiController@klintung')
->name('klintung.index');
Route::get('/home', 'DestinasiController@klintung')
->name('klintung.index');
Route::get('/detail_destinasi/{destinasi}', 'DestinasiController@detail')
->name('klintung.detailv1');
Route::post('/detail_destinasi/{destinasi}', 'RiviewController@simpan')
->name('klintung.detailv1');

Route::get('/destinasi/create', 'DestinasiController@create')
->name('destinasi.create')->middleware('login_auth');
Route::post('/destinasi', 'DestinasiController@store')
->name('destinasi.store')->middleware('login_auth');
Route::get('/destinasi', 'DestinasiController@index')
->name('destinasi.index')->middleware('login_auth');
Route::get('/destinasi/{destinasi}', 'DestinasiController@show')
->name('destinasi.show')->middleware('login_auth');
Route::get('/destinasi/{destinasi}/edit', 'DestinasiController@edit')
->name('destinasi.edit')->middleware('login_auth');
Route::patch('/destinasi/{destinasi}', 'DestinasiController@update')
->name('destinasi.update')->middleware('login_auth');
Route::delete('/destinasi/{destinasi}', 'DestinasiController@destroy')
->name('destinasi.destroy')->middleware('login_auth');

Route::get('/datauser/create', 'DatauserController@create')
->name('datauser.create');
Route::post('/datauser', 'DatauserController@store')
->name('datauser.store');
Route::get('/datauser', 'DatauserController@index')
->name('datauser.index');
Route::get('/datauser/{datauser}', 'DatauserController@show')
->name('datauser.show');
Route::get('/datauser/{datauser}/edit', 'DatauserController@edit')
->name('datauser.edit');
Route::patch('/datauser/{datauser}', 'DatauserController@update')
->name('datauser.update');
Route::delete('/datauser/{datauser}', 'DatauserController@destroy')
->name('datauser.destroy');

Route::group(['middleware' => 'auth'], function() {
    Route::get('dashboard/user', 'DatauserController@index')
    ->name('dashboard.user')->middleware('login_auth');
    Route::get('dashboard/datawisata', 'DestinasiController@index')
    ->name('dashboard.datawisata')->middleware('login_auth');
});
Route::get('/login', 'AdminController@index')->name('login.index');
Route::get('/logout', 'AdminController@logout')->name('login.logout');
Route::post('/login', 'AdminController@process')->name('login.process');
