@extends('admin_layouts.app')
@section('header')
@include('admin_layouts.header')
@endsection
@section('content')
@php
    use App\Booking;
    $bookings=booking::all();
@endphp
<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <!--************************************
					Dashboard Banner Start
			*************************************-->
    <div class="listar-dashboardbanner">
        <ol class="listar-breadcrumb">
            <li><a href="javascript:void(0);">Home</a></li>
            <li class="listar-active">Booking</li>
        </ol>
        <h1>Booking</h1>

    </div>
    <!--************************************
					Dashboard Banner End
			*************************************-->
    <!--************************************
					Dashboard Content Start
			*************************************-->
    <div id="listar-content" class="listar-content">
        <form class="listar-formtheme listar-formusers">
            <fieldset>
                <table class="table listar-tableusers">
                    <thead>
                        <tr>
                            <th>Kode Booking</th>
                            <th>Email</th>
                            <th>Destinasi</th>
                            <th>Nomor HP</th>
                            <th>Tanggal Booking</th>
                            <th>Jumlah Tiket</th>
                            <th>Total Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bookings as $item)


                        <tr>
                            <td data-title="Kode Booking">{{ $item->kode_booking }}</td>
                            <td data-title="Email">{{ $item->email_user }}</td>
                            <td data-title="Destinasi">{{ $item->destinasi }}</td>
                            <td data-title="Nomor HP">{{ $item->no_hp }}</td>
                            <td data-title="Tanggal Booking">{{ $item->tgl_booking }}</td>
                            <td data-title="Jumlah Tiket">{{ $item->jumlah_tiket }}</td>
                            <td data-title="Total Harga">{{ $item->total_harga }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </fieldset>
        </form>
    </div>
    <!--************************************
						Dashboard Content End
			*************************************-->
</main>
<!--************************************
					Main End
		*************************************-->
@endsection
