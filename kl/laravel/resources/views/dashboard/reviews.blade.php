@extends('admin_layouts.app')
@section('header')
@include('admin_layouts.header')
@endsection
@section('content')
@php
use App\Riview;
$repiew=Riview::all();
$riviews=Riview::count();
@endphp
<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <!--************************************
					Dashboard Banner Start
			*************************************-->
    <div class="listar-dashboardbanner">
        <ol class="listar-breadcrumb">
            <li><a href="javascript:void(0);">Home</a></li>
            <li class="listar-active">Reviews</li>
        </ol>
        <h1>Reviews</h1>

    </div>
    <!--************************************
					Dashboard Banner End
			*************************************-->
    <!--************************************
					Dashboard Content Start
			*************************************-->
    <div id="listar-content" class="listar-content">
        <form class="listar-formtheme listar-formaddlisting">
            <fieldset>
                <div class="listar-dashboardreviews">
                    <div class="listar-boxtitle">
                        <h3>Basic Information</h3>

                    </div>
                    <div class="listar-dashboardreviewstabcontent tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <ul class="listar-comments">
                                <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
                                    @foreach ($repiew as $pp)


                                    <li>
                                        <div class="listar-comment">
                                            <div class="listar-commentauthorbox">
                                                <figure><img style="width: 100px" src="{{asset('laravel/public/download/'.$pp->gambar)}}"
                                                            alt="image description"></figure>
                                                <div class="listar-authorinfo">
                                                    <em>{{ $pp->destinasi }}</em>
                                                    @for ($i = 0; $i < $pp->bintang; $i++)
                                                    <i style="color: gold" class="fas fa-star"></i>
                                                    @endfor

                                                </div>
                                            </div>
                                            <div class="listar-commentcontent">
                                                <time datetime="2017-09-09">
                                                    <i class="icon-alarmclock"></i>
                                                    <span>{{ $pp->created_at }}</span>
                                                </time>
                                                <div class="listar-description">
                                                    <p>{{ $pp->komentar }}</p>
                                                    <ul class="listar-authorgallery">
                                                        <li>
                                                            <figure><img
                                                                        src="{{asset('laravel/public/download/'.$pp->gambar)}}"
                                                                        alt="image description"
                                                                        style="width: 200px"></figure>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <ul class="listar-comments">
                                <li>
                                    <div class="listar-comment">
                                        <div class="listar-commentauthorbox">
                                            <figure><a href="javascript:void(0);"><img src="images/img-02.jpg"
                                                        alt="image description"></a></figure>
                                            <div class="listar-authorinfo">
                                                <h3>Katie</h3>
                                                <em>Family Vacation</em>
                                                <span class="listar-stars"><span></span></span>
                                            </div>
                                        </div>
                                        <a class="listar-helpful" href="javascript:void(0);">
                                            <i class="icon-thumb-up2"></i>
                                            <span>Helpful</span>
                                            <span>5</span>
                                        </a>
                                        <div class="listar-commentcontent">
                                            <time datetime="2017-09-09">
                                                <i class="icon-alarmclock"></i>
                                                <span>January 25, 2017</span>
                                            </time>
                                            <div class="listar-description">
                                                <p>Maecenas sed diam eget risus varius blandit sit amet non magna.
                                                    Vivamus sagittis lacus vel augue Sed non mauris vitae;erat consequat
                                                    auctor eu in elit. Class aptent taciti sociosqu ad litora torquent
                                                    per conubia nostra, per inceptos himenaeos. Mauris in erat justo
                                                    First, please don’t fall sick. However, if in case something does
                                                    catchup.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="listar-comment">
                                        <div class="listar-commentauthorbox">
                                            <figure><a href="javascript:void(0);"><img src="images/img-02.jpg"
                                                        alt="image description"></a></figure>
                                            <div class="listar-authorinfo">
                                                <h3>Katie</h3>
                                                <em>Family Vacation</em>
                                                <span class="listar-stars"><span></span></span>
                                            </div>
                                        </div>
                                        <a class="listar-helpful" href="javascript:void(0);">
                                            <i class="icon-thumb-up2"></i>
                                            <span>Helpful</span>
                                            <span>1</span>
                                        </a>
                                        <div class="listar-commentcontent">
                                            <time datetime="2017-09-09">
                                                <i class="icon-alarmclock"></i>
                                                <span>January 25, 2017</span>
                                            </time>
                                            <div class="listar-description">
                                                <p>Maecenas sed diam eget risus varius blandit sit amet non magna.
                                                    Vivamus sagittis lacus vel augue Sed non mauris vitae;erat consequat
                                                    auctor eu in elit. Class aptent taciti sociosqu ad litora torquent
                                                    per conubia nostra.</p>
                                                <ul class="listar-authorgallery">
                                                    <li>
                                                        <figure><a href="images/img-03.jpg"
                                                                data-rel="prettyPhoto[userimgthree]"><img
                                                                    src="images/img-03.jpg" alt="image description"></a>
                                                        </figure>
                                                    </li>
                                                    <li>
                                                        <figure><a href="images/img-04.jpg"
                                                                data-rel="prettyPhoto[userimgthree]"><img
                                                                    src="images/img-04.jpg" alt="image description"></a>
                                                        </figure>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="listar-comment">
                                        <div class="listar-commentauthorbox">
                                            <figure><a href="javascript:void(0);"><img src="images/img-02.jpg"
                                                        alt="image description"></a></figure>
                                            <div class="listar-authorinfo">
                                                <h3>Katie</h3>
                                                <em>Family Vacation</em>
                                                <span class="listar-stars"><span></span></span>
                                            </div>
                                        </div>
                                        <a class="listar-helpful" href="javascript:void(0);">
                                            <i class="icon-thumb-up2"></i>
                                            <span>Helpful</span>
                                        </a>
                                        <div class="listar-commentcontent">
                                            <time datetime="2017-09-09">
                                                <i class="icon-alarmclock"></i>
                                                <span>January 25, 2017</span>
                                            </time>
                                            <div class="listar-description">
                                                <p>What a magical place, even better than I imagined! Teresa and
                                                    Daniella were so helpful and awesome</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="listar-comment">
                                        <div class="listar-commentauthorbox">
                                            <figure><a href="javascript:void(0);"><img src="images/img-02.jpg"
                                                        alt="image description"></a></figure>
                                            <div class="listar-authorinfo">
                                                <h3>Katie</h3>
                                                <em>Family Vacation</em>
                                                <span class="listar-stars"><span></span></span>
                                            </div>
                                        </div>
                                        <a class="listar-helpful" href="javascript:void(0);">
                                            <i class="icon-thumb-up2"></i>
                                            <span>Helpful</span>
                                            <span>1</span>
                                        </a>
                                        <div class="listar-commentcontent">
                                            <time datetime="2017-09-09">
                                                <i class="icon-alarmclock"></i>
                                                <span>January 25, 2017</span>
                                            </time>
                                            <div class="listar-description">
                                                <p>Very nice place</p>
                                                <ul class="listar-authorgallery">
                                                    <li>
                                                        <figure><a href="images/img-04.jpg"
                                                                data-rel="prettyPhoto[userimgfour]"><img
                                                                    src="images/img-04.jpg" alt="image description"></a>
                                                        </figure>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="listar-comment">
                                        <div class="listar-commentauthorbox">
                                            <figure><a href="javascript:void(0);"><img src="images/img-02.jpg"
                                                        alt="image description"></a></figure>
                                            <div class="listar-authorinfo">
                                                <h3>Katie</h3>
                                                <em>Family Vacation</em>
                                                <span class="listar-stars"><span></span></span>
                                            </div>
                                        </div>
                                        <a class="listar-helpful" href="javascript:void(0);">
                                            <i class="icon-thumb-up2"></i>
                                            <span>Helpful</span>
                                        </a>
                                        <div class="listar-commentcontent">
                                            <time datetime="2017-09-09">
                                                <i class="icon-alarmclock"></i>
                                                <span>January 25, 2017</span>
                                            </time>
                                            <div class="listar-description">
                                                <p>Maecenas sed diam eget risus varius blandit sit amet non magna.
                                                    Vivamus sagittis lacus vel augue Sed non mauris vitae;erat consequat
                                                    auctor eu in elit. Class aptent taciti sociosqu ad litora torquent
                                                    per conubia nostra justo.</p>
                                                <p>First, please don’t fall sick. However, if in case something does
                                                    catchup with you, we will airlift you to hospital but your insurance
                                                    will have to pay for this. Ulins aliquam massa nisl quis neque.
                                                    Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum
                                                    feugiat, velit mauris egestas quam, ut liquam massa nisl quis neque.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <!--************************************
						Dashboard Content End
			*************************************-->
</main>
<!--************************************
					Main End
		*************************************-->
@endsection
