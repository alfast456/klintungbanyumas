@extends('admin_layouts.app')
@section('header')
@include('admin_layouts.header')
@endsection
@section('content')
<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <!--************************************
					Dashboard Banner Start
			*************************************-->
    <div class="listar-dashboardbanner">
        <div class="listar-leftbox">
            <ol class="listar-breadcrumb">
                <li><a href="javascript:void(0);">Home</a></li>
                <li class="listar-active">Add Listing</li>
            </ol>
            <h1>Tambah Data Wisata</h1>

        </div>
    </div>
    <!--************************************
					Dashboard Banner End
			*************************************-->
    <!--************************************
					Dashboard Content Start
			*************************************-->
    <div id="listar-content" class="listar-content">
        <form class="listar-formtheme listar-formaddlisting" action="{{ route('destinasi.store') }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="listar-steptitle"><em>Basic Information</em></div>
            <section>
                <fieldset>
                    <div class="listar-boxtitle">
                        <h3>Basic Information</h3>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Nama Wisata</label>
                                <input type="text" name="nama" id="nama" class="form-control"
                                    placeholder="Salt &amp; Pepper Restaurant">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Video Url <span>(Viemo or Youtube)</span></label>
                                <input type="url" name="video" id="video" class="form-control" placeholder="//:http">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Location </label>
                                <input type="text" name="lokasi" id="lokasi" class="form-control" placeholder="Pakistan">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Maps </label>
                                <input type="text" name="maps" id="maps" class="form-control" placeholder="Pacitan">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Phone Number</label>
                                <input type="text" name="nohp_wisata" id="nohp_wisata"class="form-control"
                                    placeholder="111 - 111 - 9870">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Description</label>
                                <div class="clearfix"></div>
                                <textarea id="deskripsi" class="listar-tinymceeditor"
                                    name="deskripsi"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Gallery</label>
                                <input type="file" class="form-control-file @error('foto') is-invalid @enderror"
                                    id="foto" name="foto" value="{{ old('foto') }}">
                                @error('foto')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                                </label>
                            </div>
                        </div>

                    </div>
                </fieldset>
            </section>
            <div class="listar-steptitle"><em>Price Setting</em></div>
            <section>

                <fieldset>
                    <div class="listar-boxtitle">
                        <h3>Pricing</h3>
                    </div>
                    <div class="row">
                        <ul id="listar-sortable" class="listar-sortable">
                            <li class="listar-slot">
                                <span class="listar-arangeslot"><img
                                        src="{{ asset('/laravel/public/image/icons/icon-09.jpg') }}"
                                        alt="image description"></span>


                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                                    <div class="form-group listar-dashboardfield">
                                        <label>Price</label>
                                        <div class="listar-inputwithicon">
                                            <span>Rp</span>
                                            <input type="text" name="harga" id="harga" class="form-control" placeholder="Price">
                                        </div>
                                    </div>
                                </div>

                            </li>

                        </ul>

                    </div>
                </fieldset>
            </section>
            <div class="listar-steptitle"><em>Business Hours</em></div>
            <section>
                <fieldset>
                    <div class="listar-boxtitle">
                        <h3>Business Hours</h3>
                    </div>
                    <div class="row">
                        <ul class="listar-businesshours">
                            <li>
                                <label>Monday</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="jam" name="jam" />
                                </div>

                            </li>
                            <li>
                                <label>Tuesday</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="jam2" name="jam2" />
                                </div>

                            </li>
                            <li>
                                <label>Wednesday</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="jam3" name="jam3" />
                                </div>

                            </li>
                            <li>
                                <label>Thursday</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="jam4" name="jam4" />
                                </div>

                            </li>
                            <li>
                                <label>Friday</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="jam5" name="jam5" />
                                </div>

                            </li>
                            <li>
                                <label>Saturday</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="jam6" name="jam6" />
                                </div>

                            </li>
                            <li>
                                <label>Sunday</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="jam7" name="jam7" />
                                </div>

                            </li>
                            <li>
                                <label>Close</label>
                                <div class="listar-dashboardfield">
                                    <input type="time" name="tutup" name="tutup" />
                                </div>

                            </li>
                        </ul>
                    </div>
                </fieldset>
            </section>


            <button type="submit" class="listar-btn listar-btnblue">add</button>
        </form>
    </div>
    <!--************************************
						Dashboard Content End
			*************************************-->
</main>
<!--************************************
					Main End
		*************************************-->
@endsection
