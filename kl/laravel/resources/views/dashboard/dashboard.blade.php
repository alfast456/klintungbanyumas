@extends('admin_layouts.app')
@section('header')
@include('admin_layouts.header')
@endsection
@section('content')
@php
    use App\Destinasi;
    $wisatas=destinasi::count();
    use App\Datauser;
    $users=datauser::count();
    use App\Booking;
    $bookings=booking::count();
    use App\Riview;
    $reviews=riview::count();
@endphp
<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <!--************************************
					Dashboard Banner Start
			*************************************-->
    <div class="listar-dashboardbanner">
        <ol class="listar-breadcrumb">
            <li><a href="javascript:void(0);">Home</a></li>
            <li class="listar-active">Dashboard</li>
        </ol>
        <h1>Dashboard</h1>

    </div>
    <!--************************************
					Dashboard Banner End
			*************************************-->
    <!--************************************
					Dashboard Alert Start
			*************************************-->
    <!-- <div class="lisatr-alert alert alert-success fade in alert-dismissable">
				<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
				<strong>Success!</strong> This alert box indicates a successful or positive action.
				<a class="listar-btnaction" href="javascript:void(0);">Do Action Now</a>
			</div> -->
    <!--************************************
					Dashboard Alert End
			*************************************-->
    <!--************************************
					Dashboard Content Start
			*************************************-->
    <div id="listar-content" class="listar-content">
        <form class="listar-formtheme listar-formaddlisting listar-formdashboard">
            <div class="row">
                <fieldset class="listar-statisticsarea">
                    <ul class="listar-statistics">
                        <li class="listar-activelists">
                            <div class="listar-couterholder">
                                <h3 data-from="0" data-to="{{ $wisatas }}" data-speed="8000" data-refresh-interval="2000">{{ $wisatas }}</h3>
                                <h4>Data Wisata</h4>
                                <div class="listar-statisticicon"><i class="icon-map3"></i></div>
                            </div>
                        </li>
                        <li class="listar-newuser">
                            <div class="listar-couterholder">
                                <h3 data-from="0" data-to="{{ $users }}" data-speed="8000" data-refresh-interval="2000">{{ $users }}</h3>
                                <h4>Data User</h4>
                                <div class="listar-statisticicon"><i class="icon-user2"></i></div>
                            </div>
                        </li>
                        <li class="listar-weeksviews">
                            <div class="listar-couterholder">
                                <h3 data-from="0" data-to="{{ $bookings }}" data-speed="8000" data-refresh-interval="2000">{{ $bookings }}</h3>
                                <h4>Booking</h4>
                                <div class="listar-statisticicon"><i class="icon-linegraph"></i></div>
                            </div>
                        </li>
                        <li class="listar-newfeedback">
                            <div class="listar-couterholder">
                                <h3 data-from="0" data-to="{{ $reviews }}" data-speed="8000" data-refresh-interval="2000">{{ $reviews }}</h3>
                                <h4>Reviews</h4>
                                <div class="listar-statisticicon"><i class="icon-bubble3"></i></div>
                            </div>
                        </li>
                    </ul>
                </fieldset>

            </div>
        </form>
    </div>
    <!--************************************
						Dashboard Content End
			*************************************-->
</main>
<!--************************************
					Main End
		*************************************-->
@endsection
