@extends('admin_layouts.app')
@section('header')
@include('admin_layouts.header')
@endsection
@section('content')

<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <!--************************************
					Dashboard Banner Start
			*************************************-->
    <div class="listar-dashboardbanner">
        <ol class="listar-breadcrumb">
            <li><a href="javascript:void(0);">Home</a></li>
            <li class="listar-active">data_wisata</li>
        </ol>
        <h1>Data Wisata</h1>

    </div>
    <!--************************************
					Dashboard Banner End
			*************************************-->
    <!--************************************
					Dashboard Content Start
			*************************************-->
    <div id="listar-content" class="listar-content">
        @if(session()->has('pesan'))
        <div class="alert alert-success">
            {{ session()->get('pesan') }}
        </div>
        @endif
        <form class="listar-formtheme listar-formaddlisting listar-formwishlist">
            <fieldset>
                <div class="listar-boxtitle">

                    <ul class="listar-actionbtns" role="tablist">
                        <a id="listar-btnsignin" class="listar-btn listar-btnblue" href="addwisata">
                            <span>Tambah Data</span>
                        </a>

                    </ul>
                </div>
                <div class="listar-dashboardwishlists tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        @forelse ($destinasis as $wisata)
                        <div class="listar-themepost listar-placespost">

                            <a class="listar-btnedite" href="{{ route('destinasi.edit',['destinasi' => $wisata->id]) }}"><i class="icon-pencil4"></i></a>
                            <form action="{{ route('destinasi.destroy',['destinasi'=>$wisata->id]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="listar-btndelpost">x</button>
                            </form>
                            {{-- <a class="listar-btndelpost" href="javascript:void(0);">x</a> --}}
                            <figure class="listar-featuredimg"><a href="javascript:void(0);">
                                    <img style="width: 100px" src="{{asset('laravel/public/download/'.$wisata->foto)}}"
                                        alt="image description" class="mCS_img_loaded"></a>
                            </figure>
                            <div class="listar-postcontent">
                                <h3><a href="javascript:void(0);">{{$wisata->nama}}</a></h3>
                                <span class="listar-catagory">{{$wisata->lokasi}}</span>
                                <div class="listar-reviewcategory">
                                    <div class="listar-review">
                                        <span class="listar-stars"></span>
                                        <em></em>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <div colspan="8" class="text-center">Tidak ada data...</div>
                        @endforelse

                    </div>

                </div>
            </fieldset>
        </form>
    </div>
    <!--************************************
						Dashboard Content End
			*************************************-->
</main>
<!--************************************
					Main End
		*************************************-->

@endsection
