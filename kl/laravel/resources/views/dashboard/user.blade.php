@extends('admin_layouts.app')
@section('header')
@include('admin_layouts.header')
@endsection
@section('content')
<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <!--************************************
					Dashboard Banner Start
			*************************************-->
            @if(session()->has('pesan'))
            <div class="alert alert-success">
                {{ session()->get('pesan') }}
            </div>
            @endif
            <div class="listar-dashboardbanner">
        <ol class="listar-breadcrumb">
            <li><a href="javascript:void(0);">Home</a></li>
            <li class="listar-active">Users List</li>
        </ol>
        <h1>Data User</h1>

    </div>
    <!--************************************
					Dashboard Banner End
			*************************************-->
    <!--************************************
					Dashboard Content Start
			*************************************-->

    <div id="listar-content" class="listar-content">
        <form class="listar-formtheme listar-formusers">
            <fieldset>
                <table class="table listar-tableusers">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>EMAIL</th>
                            <th>NAMA</th>
                            <th>PHONE NUMBER</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($datausers as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->nama}}</td>
                            <td>{{$user->no_hp}}</td>
                        </tr>
                        @empty
                        <td colspan="5" class="text-center">Tidak ada data...</td>
                        @endforelse
                    </tbody>
                </table>

            </fieldset>
        </form>
    </div>
    <!--************************************
						Dashboard Content End
			*************************************-->
</main>
<!--************************************
					Main End
		*************************************-->
@endsection
