@extends('admin_layouts.app')
@section('header')
@include('admin_layouts.header')
@endsection
@section('content')
<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <!--************************************
					Dashboard Banner Start
			*************************************-->
    <div class="listar-dashboardbanner">
        <ol class="listar-breadcrumb">
            <li><a href="javascript:void(0);">Home</a></li>
            <li class="listar-active">My Profile</li>
        </ol>
        <h1>My Profile</h1>
        <div class="listar-description">

        </div>
    </div>
    <!--************************************
					Dashboard Banner End
			*************************************-->
    <!--************************************
					Dashboard Content Start
			*************************************-->
    <div id="listar-content" class="listar-content">
        <form class="listar-formtheme listar-formaddlisting">
            <fieldset>
                <div class="listar-boxtitle">
                    <h3>My Profile</h3>
                </div>
                <div class="listar-dashboardmyprofile">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group listar-dashboardfield">
                                <label>Your Name</label>
                                <input type="text" name="name" class="form-control" placeholder="John parker">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Email Address</label>
                                <input type="email" name="email" class="form-control"
                                    placeholder="listingstar@gmail.com">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>Phone Number</label>
                                <input type="text" name="phonenumber" class="form-control" placeholder="013214577">
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <div class="listar-boxtitle">
                    <h3>Update Password</h3>
                </div>
                <div class="listar-dashboardpassword">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group listar-dashboardfield">
                                <label>New Password</label>
                                <input type="password" name="password" class="form-control"
                                    placeholder="your new password">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <a class="listar-btn listar-btngreen" href="javascript:void(0);">Update Profile</a>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <!--************************************
						Dashboard Content End
			*************************************-->
</main>
<!--************************************
					Main End
		*************************************-->
@endsection
