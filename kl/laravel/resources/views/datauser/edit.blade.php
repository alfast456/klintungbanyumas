<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link href="../../laravel/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<title>Edit Mahasiswa</title>
	</head>
	<body>
		<div class="container pt-4 bg-white">
			<div class="row">
				<div class="col-md-8 col-xl-6">
					<h1>Edit user</h1>
					<hr>
					<form action="{{ route('datauser.update',['datauser' => $datauser->id]) }}" method="POST" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf <div class="form-group">
							<label for="nama">nama</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') ?? $datauser->nama }}"> @error('nama') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label for="no_hp">no_hp Lengkap</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="no_hp" name="no_hp" value="{{ old('no_hp') ?? $datauser->no_hp }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="email">email Lengkap</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="email" name="email" value="{{ old('email') ?? $datauser->email }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="password">password Lengkap</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="password" name="password" value="{{ old('password') ?? $datauser->password }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
						<button type="submit" class="btn btn-primary mb-2">Update</button>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
