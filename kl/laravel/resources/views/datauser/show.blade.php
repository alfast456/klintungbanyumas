<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../laravel/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Biodata {{$destinasi->nama}}</title>
</head>

<body>
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="pt-3 d-flex justify-content-end align-items-center">
                    <h1 class="h2 mr-auto">Biodata {{$destinasi->nama}}</h1>
                    <a href="{{ route('destinasi.edit',['destinasi' => $destinasi->id]) }}" class="btn btn-warning">Edit
                    </a>
                    <form action="{{ route('destinasi.destroy',['destinasi'=>$destinasi->id]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger ml-3">Hapus</button>
                    </form>
                </div>
                <hr>
                @if(session()->has('pesan'))
                <div class="alert alert-success">
                    {{ session()->get('pesan') }}
                </div>
                @endif
                <ul>
                    <li>nama: {{$destinasi->nama}} </li>
                    <li>lokasi: {{$destinasi->lokasi}} </li>
                    <li>jam: {{$destinasi->jam}} </li>
                    <li>tutup: {{$destinasi->tutup}} </li>
                    <li>deskripsi:
                        {{$destinasi->deskripsi == '' ? 'N/A' : $destinasi->deskripsi}}
                    </li>
                    <li>foto:{{$destinasi->foto}}
                    </li>
                    <li>harga: {{$destinasi->harga}} </li>
                </ul>
                <a href="{{ route('destinasi.index') }}" class="btn btn-primary">Kembali
                </a>
            </div>
        </div>
    </div>
</body>

</html>
