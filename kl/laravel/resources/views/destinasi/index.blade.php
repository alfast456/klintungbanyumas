<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="laravel/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Data wisata</title>
</head>

<body>
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="py-4 d-flex justify-content-end align-items-center">
                    <h2 class="mr-auto">Tabel wisata</h2>
                    <a href="{{ route('destinasi.create') }}" class="btn btn-primary">
                        Tambah wisata
                    </a>
                    <a href="{{ route('login.logout') }}" class="btn btn-danger">
                        Logout
                    </a>
                </div>
                @if(session()->has('pesan'))
                <div class="alert alert-success">
                    {{ session()->get('pesan') }}
                </div>
                @endif
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>nama</th>
                            <th>lokasi</th>
                            <th>Jam</th>
                            <th>tutup</th>
                            <th>deskripsi</th>
                            <th>foto</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($destinasis as $wisata)
                        <tr>
                            <th>{{$wisata->id}}</th>
                            <td><a href="{{ route('destinasi.show',['destinasi' => $wisata->id]) }}">{{$wisata->nama}}</a></td>
                            <td>{{$wisata->lokasi}}</td>
                            <td>{{$wisata->jam}}</td>
                            <td>{{$wisata->tutup}}</td>
                            <td>{{$wisata->deskripsi == '' ? 'N/A' : $wisata->deskripsi}}</td>
                            <td><img style="width: 100px" src="{{asset('laravel/public/download/'.$wisata->foto)}}"></td>
                            <td>{{$wisata->harga}}</td>
                        </tr>
                        @empty
                        <td colspan="8" class="text-center">Tidak ada data...</td>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
