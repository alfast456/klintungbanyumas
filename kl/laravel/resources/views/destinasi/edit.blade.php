<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link href="../../laravel/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<title>Edit {{$destinasi->nama}}</title>
	</head>
	<body>
		<div class="container pt-4 bg-white">
			<div class="row">
				<div class="col-md-8 col-xl-6">
					<h1>Edit {{$destinasi->nama}}</h1>
					<hr>
					<form action="{{ route('destinasi.update',['destinasi' => $destinasi->id]) }}" method="POST" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf <div class="form-group">
							<label for="nama">Nama Wisata</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') ?? $destinasi->nama }}"> @error('nama') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label for="lokasi">Lokasi</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="lokasi" name="lokasi" value="{{ old('lokasi') ?? $destinasi->lokasi }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label for="maps">Maps</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="maps" name="maps" value="{{ old('maps') ?? $destinasi->maps }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="nohp_wisata">Nomor HP</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="nohp_wisata" name="nohp_wisata" value="{{ old('nohp_wisata') ?? $destinasi->nohp_wisata }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="video">Video</label>
							<input type="text" class="form-control @error('name') is-invalid @enderror" id="video" name="video" value="{{ old('video') ?? $destinasi->video }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="jam">Senin</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="jam" name="jam" value="{{ old('jam') ?? $destinasi->jam }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="jam2">Selasa</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="jam2" name="jam2" value="{{ old('jam2') ?? $destinasi->jam2 }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="jam3">Rabu</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="jam3" name="jam3" value="{{ old('jam3') ?? $destinasi->jam3 }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="jam4">Kamis</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="jam4" name="jam4" value="{{ old('jam4') ?? $destinasi->jam4 }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="jam5">Jum'at</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="jam5" name="jam5" value="{{ old('jam5') ?? $destinasi->jam5 }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="jam6">Sabtu</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="jam6" name="jam6" value="{{ old('jam6') ?? $destinasi->jam6 }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="jam7">Minggu</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="jam7" name="jam7" value="{{ old('jam7') ?? $destinasi->jam7 }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="tutup">Jam Tutup</label>
							<input type="time" class="form-control @error('name') is-invalid @enderror" id="tutup" name="tutup" value="{{ old('tutup') ?? $destinasi->tutup }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="deskripsi">Deskripsi</label>
							<textarea class="form-control" id="deskripsi" rows="3" name="deskripsi">{{ old('deskripsi') ?? $destinasi->deskripsi}}</textarea>
						</div>
                        <div class="form-group">
							<label for="foto">Foto</label>
							<input type="file" class="form-control-file @error('name') is-invalid @enderror" id="foto" name="foto" value="{{ old('name') ?? $destinasi->foto }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
                        <div class="form-group">
							<label for="harga">Harga Tiket</label>
							<input type="number" class="form-control @error('name') is-invalid @enderror" id="harga" name="harga" value="{{ old('name') ?? $destinasi->harga }}"> @error('name') <div class="text-danger">{{ $message }}</div> @enderror
						</div>
						<button type="submit" class="btn btn-primary mb-2">Update</button>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
