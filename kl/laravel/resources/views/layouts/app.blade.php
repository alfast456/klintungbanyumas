<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="zxx"> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang="zxx"> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang="zxx"> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang="zxx"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Klintung Banyumas</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="{{ asset('/laravel/public/image/klintung-banyumas-1.png') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/bootstrap-slider.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/icomoon.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/chosen.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/prettyPhoto.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/scrollbar.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/morris.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/YouTubePopUp.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/auto-complete.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/jquery.navhideshow.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/transitions.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/style.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/color.css') }}">
	<link rel="stylesheet" href="{{ asset('/laravel/public/css/responsive.css') }}">
	<script src="{{ asset('/laravel/public/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>
<body class="listar-home listar-homeone">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Preloader Start
	*************************************-->
	<div class="preloader-outer">
		<div class="pin"></div>
		<div class="pulse"></div>
	</div>
	<!--************************************
			Preloader End
	*************************************-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="listar-wrapper" class="listar-wrapper listar-haslayout">
    @yield('header')

    <!-- Content Wrapper. Contains page content -->
    @yield('content')
    <!-- /.content-wrapper -->
    		<!--************************************
				Footer Start
		*************************************-->
		<footer id="listar-footer" class="listar-footer listar-haslayout">
			<div class="listar-footeraboutarea">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="listar-upperbox">
								<strong class="listar-logo"><a href="javascript:void(0);"><img src="{{ asset('/laravel/public/image/logo.png') }}" alt="image description"></a></strong>

							</div>
							<div class="listar-lowerbox">
								<div class="listar-description">
									<p>Tentang Kami</p>
									<p>Klintung Banyumas merupakan website yang menyediakan informasi seputar pariwisata khusus didaerah Banyumas.</p>
								</div>
								<address><strong>Address:</strong> Institut Teknologi Telkom Purwokerto <span><strong>Tel:</strong> 0281-641629</span></address>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="listar-footerbar">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<span class="listar-copyright">Copyright &copy; 2021 Klintung Banyumas. All rights reserved.</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--************************************
				Footer End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Theme Modal Box Start
	*************************************-->

	<!--************************************
			Theme Modal Box End
	*************************************-->
	<!--************************************
			Login Singup Start
	*************************************-->
	
	<script src="{{ asset('/laravel/public/js/vendor/jquery-library.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/vendor/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/mapclustering/data.js') }}on"></script>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&amp;language=en"></script>
	<script src="{{ asset('/laravel/public/js/tinymce/tinymce.min.js') }}?apiKey=4cuu2crphif3fuls3yb1pe4qrun9pkq99vltezv2lv6sogci"></script>
	<script src="{{ asset('/laravel/public/js/mapclustering/markerclusterer.min.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/mapclustering/infobox.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/mapclustering/map.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/ResizeSensor.js.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/jquery.sticky-sidebar.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/YouTubePopUp.jquery.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/jquery.navhideshow.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/backgroundstretch.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/jquery.sticky-kit.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/bootstrap-slider.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/jquery.vide.min.js') }}"></script>
	<script src="{{ asset('/laravel/public/JS/auto-complete.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/chosen.jquery.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/scrollbar.min.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/isotope.pkgd.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/jquery.steps.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/prettyPhoto.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/raphael-min.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/parallax.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/sortable.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/countTo.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/appear.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/gmap3.js') }}"></script>
	<script src="{{ asset('/laravel/public/js/dev_themefunction.js') }}"></script>
</body>
</html>