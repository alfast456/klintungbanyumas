		<!--************************************
				Header Start
		*************************************-->
		<header id="listar-header" class="listar-header listar_darkheader listar-fixedheader listar-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<strong class="listar-logo"><a href="{{ url('/home') }}"><img src="{{ asset('/laravel/public/image/logo.png') }}" alt="company logo here"></a></strong>
						<nav class="listar-addnav">
							<ul>
								<li>
                                    @if (Auth::check())
                                    <a id="listar-btnsignin" class="listar-btn listar-btnblue" href="{{ url('/profile') }}">
										<i class="icon-user"></i>
										<span>my profile</span>
									</a>
                                    @else
                                    <a id="listar-btnsignin" class="listar-btn listar-btnblue" href="{{ url('/login') }}">
										<i class="icon-smiling-face"></i>
										<span>Join Now</span>
									</a>
                                    @endif
								</li>

							</ul>
						</nav>
						<nav id="listar-nav" class="listar-nav">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#listar-navigation" aria-expanded="false">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div id="listar-navigation" class="collapse navbar-collapse listar-navigation">
								<ul>
									<li class="menu-item-has-children current-menu-item">
										<a href="{{ url('/home') }}">Home</a>
									</li>
									<li class="menu-item-has-children">
										<a href="{{ url('/explore') }}">Explore</a>
									</li>
									<li><a href="{{ url('/contact') }}">Contact Us</a></li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!--************************************
				Header End
		*************************************-->
