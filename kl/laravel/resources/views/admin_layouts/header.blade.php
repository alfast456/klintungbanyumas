<!--************************************
				Header Start
		*************************************-->
<header id="listar-dashboardheader" class="listar-dashboardheader listar-haslayout">
    <div class="cd-auto-hide-header listar-haslayout">
        <div class="container-fluid">
            <div class="row">
                <strong class="listar-logo"><a href="index.html"><img
                            src="{{ asset('/laravel/public/image/logo.png') }}" alt="company logo here"></a></strong>

            </div>
        </div>
    </div>
    <div id="listar-sidebarwrapper" class="listar-sidebarwrapper">
        <strong class="listar-logo"><a href="index.html"><img src="{{ asset('/laravel/public/image/logo.png') }}"
                    alt="company logo here"></a></strong>
        <span id="listar-btnmenutoggle" class="listar-btnmenutoggle"><i class="fa fa-angle-left"></i></span>
        <div id="listar-verticalscrollbar" class="listar-verticalscrollbar">
            <nav id="listar-navdashboard" class="listar-navdashboard">
                <div class="listar-menutitle"><span>Main</span></div>
                <ul>
                    <li>
                        <a href="{{ url('dashboard') }}">
                            <i class="icon-speedometer2"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/datawisata') }}">
                            <i class="icon-profile-male"></i>
                            <span>Data Wisata</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/user') }}">
                            <i class="icon-layers"></i>
                            <span>Data User</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/booking') }}">
                            <i class="icon-pencil3"></i>
                            <span>Booking</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/reviews') }}">
                            <i class="icon-star4"></i>
                            <span>Reviews</span>
                        </a>
                    </li>


                </ul>
                <div class="listar-menutitle listar-menutitleaccount"><span>Account</span></div>
                <ul>
                    <li>
                        <a href="{{ url('dashboard/myprofile') }}">
                            <i class="icon-lock6"></i>
                            <span>My Profile</span>
                        </a>
                        <a href="{{ url('/logout') }}">
                            <i class="icon-logout"></i>
                            <span>log out</span>
                        </a>
                    </li>
                    <!-- <li>
								<a href="dashboard-profile-setting.html">
									<i class="icon-user4"></i>
									<span>Logout</span>
								</a>
							</li> -->
                </ul>
            </nav>
        </div>
    </div>
</header>
<!--************************************
				Header End
		*************************************-->
