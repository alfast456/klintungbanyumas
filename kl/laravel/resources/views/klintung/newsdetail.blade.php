@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')
		<!--************************************
				Inner Search Start
		*************************************-->
		<div class="listar-innerpagesearch">
			<a id="listar-btnsearchtoggle" class="listar-btnsearchtoggle" href="javascript:void(0);"><i class="icon-icons185"></i></a>
			<div id="listar-innersearch" class="listar-innersearch">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<form class="listar-formtheme listar-formsearchlisting">
								<fieldset>
									<div class="form-group listar-inputwithicon">
										<i class="icon-layers"></i>
										<div class="listar-select">
											<select id="listar-categorieschosen" class="listar-categorieschosen listar-chosendropdown">
												<option>Ex: Food, Retail, hotel, cinema</option>
												<option class="icon-entertainment">Art &amp; Entertainment</option>
												<option class="icon-shopping">Beauty &amp; Health</option>
												<option class="icon-study">Education</option>
												<option class="icon-healthfitness">Fitness</option>
												<option class="icon-icons240">Hotels</option>
												<option class="icon-localservice">Motor Mechanic</option>
												<option class="icon-nightlife">Night Life</option>
												<option class="icon-tourism">Restaurant</option>
												<option class="icon-shopping">Real Estate</option>
												<option class="icon-shopping">Shopping</option>
											</select>
										</div>
									</div>
									<div class="form-group listar-inputwithicon">
										<i class="icon-global"></i>
										<div class="listar-select listar-selectlocation">
											<select id="listar-locationchosen" class="listar-locationchosen listar-chosendropdown">
												<option>Choose a Location</option>
												<option>Lahore</option>
												<option>Bayonne</option>
												<option>Greenville</option>
												<option>Manhattan</option>
												<option>Queens</option>
												<option>The Heights</option>
											</select>
										</div>
									</div>
									<div class="form-group listar-inputwithicon">
										<i class=""><img src="images/icons/icon-01.png" alt="image description"></i>
										<p>Price: </p>
										<!-- <input id="listar-rangeslider" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14"> -->
										<input id="listar-rangeslider" class="listar-rangeslider" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14">
									</div>
									<button type="button" class="listar-btn listar-btngreen">Search Places</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Search End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="listar-main" class="listar-main listar-haslayout">
			<div id="listar-twocolumns" class="listar-twocolumns">
				<div class="listar-themepost listar-post listar-detail listar-postdetail">
					<figure class="listar-featuredimg">
						<img src="images/blog/img-22.jpg" alt="image description">
						<figcaption>
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="listar-postcontent">
											<div class="listar-postauthordpname">
												<span class="listar-postauthordp"><a href="javascript:void(0);"><img src="images/author/img-05.jpg" alt="image description"></a></span>
												<span class="listar-postauhorname"><a href="javascript:void(0);">Johny bravo</a></span>
											</div>
											<time datetime="2017-08-08">
												<i class="icon-clock4"></i>
												<span>Apr 22, 2018</span>
											</time>
											<span class="listar-postcomment">
												<i class="icon-comment"></i>
												<span>3 Comments</span>
											</span>
											<div class="listar-btnquickinfo">
												<div class="listar-shareicons">
													<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
													<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
													<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
												</div>
												<a class="listar-btnshare" href="javascript:void(0);">
													<i class="icon-share3"></i>
													<span>Share</span>
												</a>
											</div>
											<h1>We have Lot's of Success Stories</h1>
										</div>
									</div>
								</div>
							</div>
						</figcaption>
					</figure>
					<div class="clearfix"></div>
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-push-1 col-md-10 col-lg-push-1 col-lg-10">
								<div id="listar-detailcontent" class="listar-detailcontent">
									<div class="listar-description">
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop <a href="javascript:void(0);">publishing packages</a> and web page editors.</p>
										<p>Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident on purpose (injected humour and the like).</p>
										<h2>All Stunning Places</h2>
										<ol class="listar-orderlist">
											<li> It is a long established fact that a reader will be distracted by the readable content.</li>
											<li> Lorem Ipsum their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have  many web sites still in their infancy.</li>
											<li> It is a long established fact that a reader will be distracted by the readable content.</li>
										</ol>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ofevolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
										<blockquote><q>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue laoreet.</q>
										</blockquote>
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
												<figure class="listar-imgbox"><img src="images/blog/img-23.jpg" alt="image description"></figure>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
												<figure class="listar-imgbox"><img src="images/blog/img-24.jpg" alt="image description"></figure>
											</div>
										</div>
										<h2>Text that where it came from it</h2>
										<p>It is a long established fact that a reader will be distracted by the <a href="javascript:void(0);">readable content</a> of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ofevolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
										<p></p>
									</div>
									<div class="listar-nextprevposts">
										<div class="listar-prevpost">
											<a href="#">
												<i class="fa fa-angle-left"></i><span>Previous Reading</span>
												<h2>Get Fit on The Go</h2>
											</a>
										</div>
										<div class="listar-nextpost">
											<a href="#">
												<span>Next Reading</span><i class="fa fa-angle-right"></i>
												<h2>City Tours in Europe</h2>
											</a>
										</div>
									</div>
									<section class="listar-comments">
										<div class="listar-heading listar-headingvtwo">
											<h2>2 Responses</h2>
										</div>
										<ul id="listar-comments" class="listar-comments">
											<li>
												<div class="listar-comment">
													<figure><img src="images/author/img-09.jpg" alt="image description"></figure>
													<div class="listar-content">
														<div class="listar-commenthead">
															<div class="listar-author">
																<h3>John Smith</h3>
																<time datetime="2017-12-12">December 21, 2017 at 3:04 pm</time>
															</div>
															<a class="listar-reply" href="javascript:void(0);">reply</a>
														</div>
														<div class="listar-description">
															<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
														</div>
													</div>
												</div>
											</li>
											<li>
												<div class="listar-comment">
													<figure><img src="images/author/img-09.jpg" alt="image description"></figure>
													<div class="listar-content">
														<div class="listar-commenthead">
															<div class="listar-author">
																<h3>John Smith</h3>
																<time datetime="2017-12-12">December 21, 2017 at 3:04 pm</time>
															</div>
															<a class="listar-reply" href="javascript:void(0);">reply</a>
														</div>
														<div class="listar-description">
															<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</section>
									<section class="listar-formreviewarea">
										<form class="listar-formtheme listar-formaddreview">
											<fieldset>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="form-group">
															<input type="text" name="yourname" class="form-control" placeholder="Your Name">
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="form-group">
															<input type="text" name="emailaddress" class="form-control" placeholder="Email Address">
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="form-group">
															<input type="text" name="website" class="form-control" placeholder="Website">
														</div>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group">
															<textarea name="message" class="form-control" placeholder="Message"></textarea>
														</div>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<button class="listar-btn listar-btngreen" type="button">Post Comment</button>
													</div>
												</div>
											</fieldset>
										</form>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!--************************************
				Main End
		*************************************-->
		@endsection

	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Login Singup Start
	*************************************-->
	<div id="listar-loginsingup" class="listar-loginsingup">
		<button type="button" class="listar-btnclose">x</button>
		<figure class="listar-loginsingupimg" data-vide-bg="poster: images/bgjoin.jpg" data-vide-options="position: 50% 50%"></figure>
		<div class="listar-contentarea">
			<div class="listar-themescrollbar">
				<div class="listar-logincontent">
					<div class="listar-themetabs">
						<ul class="listar-tabnavloginregistered" role="tablist">
							<li role="presentation" class="active"><a href="#listar-loging" data-toggle="tab">Log in</a></li>
							<li role="presentation"><a href="#listar-register" data-toggle="tab">Register</a></li>
						</ul>
						<div class="tab-content listar-tabcontentloginregistered">
							<div role="tabpanel" class="tab-pane active fade in" id="listar-loging">
								<form class="listar-formtheme listar-formlogin">
									<fieldset>
										<div class="form-group listar-inputwithicon">
											<i class="icon-profile-male"></i>
											<input type="text" name="username" class="form-control" placeholder="Username Or Email">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-icons208"></i>
											<input type="password" name="password" class="form-control" placeholder="Password">
										</div>
										<div class="form-group">
											<div class="listar-checkbox">
												<input type="checkbox" name="remember" id="rememberpass2">
												<label for="rememberpass2">Remember me</label>
											</div>
											<span><a href="#">Lost your Password?</a></span>
										</div>
										<button class="listar-btn listar-btngreen">Register</button>
									</fieldset>
								</form>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="listar-register">
								<form class="listar-formtheme listar-formlogin">
									<fieldset>
										<div class="form-group listar-inputwithicon">
											<i class="icon-profile-male"></i>
											<input type="text" name="username" class="form-control" placeholder="Username">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-icons208"></i>
											<input type="email" name="emailaddress" class="form-control" placeholder="Email Address">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-lock-stripes"></i>
											<input type="password" name="password" class="form-control" placeholder="Password">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-lock-stripes"></i>
											<input type="password" name="confirmpassword" class="form-control" placeholder="Password">
										</div>
										<div class="form-group">
											<div class="listar-checkbox">
												<input type="checkbox" name="remember" id="rememberpass">
												<label for="rememberpass">Remember me</label>
											</div>
											<span><a href="#">Lost your Password?</a></span>
										</div>
										<button class="listar-btn listar-btngreen">login</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="listar-shareor"><span>or</span></div>
					<div class="listar-signupwith">
						<h2>Sign in With...</h2>
						<ul class="listar-signinloginwithsocialaccount">
							<li class="listar-facebook"><a href="javascript:void(0);"><i class="icon-facebook-1"></i><span>Facebook</span></a></li>
							<li class="listar-twitter"><a href="javascript:void(0);"><i class="icon-twitter-1"></i><span>Twitter</span></a></li>
							<li class="listar-googleplus"><a href="javascript:void(0);"><i class="icon-google4"></i><span>Google +</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--************************************
			Login Singup End
	*************************************-->
	<!--************************************
			Theme Modal Box Start
	*************************************-->
	<div class="modal fade listar-placequickview" tabindex="-1" role="dialog">
		<div class="modal-dialog listar-modaldialog" role="document">
			<div class="modal-content listar-modalcontent">
				<div class="listar-themepost listar-placespost">
					<span class="listar-btnclosequickview" data-toggle="modal" data-target=".listar-placequickview">X</span>
					<figure class="listar-featuredimg" data-vide-bg="poster: images/post/img-16.jpg" data-vide-options="position: 50% 50%">
						<span class="listar-contactnumber">
							<i class="icon-"><img src="images/icons/icon-03.png" alt="image description"></i>
							<em> + 7890 456 133</em>
						</span>
					</figure>
					<div class="listar-postcontent">
						<h3><a href="javascript:void(0);">Serena Hotel</a><i class="icon-checkmark listar-postverified listar-themetooltip" data-toggle="tooltip" data-placement="top" title="Verified"></i></h3>
						<div class="listar-description">
							<p>Ut euismod ultricies sollicitudin. Curabitur sed dapibus nulla. Nulla eget iaculis lectus. Mauris ac maximus neque. Nam in mauris quis libero sodales eleifend. Morbi varius, nulla sit Nam in mauris quis libero sodales eleifend amet rutrum elementum, est elit finibus tellus, ut tristique elit risus at metus</p>
						</div>
						<ul class="listar-listfeatures">
							<li>Pets allowed</li>
							<li>Kitchen</li>
							<li>Internet</li>
							<li>Suitable for events</li>
							<li>Gym</li>
							<li>Dryer</li>
							<li>Hot tub</li>
							<li>Family/kid friendly</li>
							<li>Wireless Internet</li>
						</ul>
						<div class="listar-reviewcategory">
							<div class="listar-review">
								<span class="listar-stars"><span></span></span>
								<em>(3 Review)</em>
							</div>
							<a href="javascript:void(0);" class="listar-category">
								<i class="icon-tourism"></i>
								<span>Hotel</span>
							</a>
						</div>
						<div class="listar-themepostfoot">
							<span class="listar-openinghours">
								<i class="icon-alarmclock2"></i>
								<em>Today <span class="listar-greenthemecolor">Open Now</span> 10:00 AM - 5:00 PM</em>
							</span>
							<div class="listar-postbtns">
								<a class="listar-btnquickinfo listar-liked" href="javascript:void(0);"><i class="icon-heart2"></i></a>
								<div class="listar-btnquickinfo">
									<div class="listar-shareicons">
										<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
										<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
										<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
									</div>
									<a class="listar-btnshare" href="javascript:void(0);">
										<i class="icon-share3"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
