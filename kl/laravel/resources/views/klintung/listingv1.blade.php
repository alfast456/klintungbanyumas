@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')

<div class="listar-homebannerslider">

</div>
<!--************************************
				Main Start
		*************************************-->

<main id="listar-main" class="listar-main listar-haslayout">
    <div id="listar-content" class="listar-content">
        <div class="listar-listing listar-listingvone">
            <div id="listar-mapclustring" class="listar-mapclustring">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63301.05091750714!2d109.2344236!3d-7.4302759!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655c3136423d1d%3A0x4027a76e352e4a0!2sPurwokerto%2C%20Kabupaten%20Banyumas%2C%20Jawa%20Tengah!5e0!3m2!1sid!2sid!4v1643342637508!5m2!1sid!2sid" width="100%" height="600px" name="iframe_a" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 pull-left">
                <div class="row">
                    <div class="listar-listingarea">
                        <div class="listar-innerpagesearch">
                            <div class="listar-innersearch">
                                <div class="listar-searchstatus">
                                    <h1>Explore Klintung Banyumas</h1>
                                </div>
                                <fieldset>
                                    <div class="form-group listar-inputwithicon">
                                        <i class="icon-icons185"></i>
                                        <form action="{{ url('explore/search') }}" method="GET">
                                        <input type="text" name="nama" class="form-control"
                                            placeholder="What are you looking for ?">
                                        </form>
                                    </div>

                                </fieldset>
                                <form class="listar-formtheme listar-formsearchlisting">


                                    <fieldset>

                                        <div class="listar-rightbox">

                                            <ul class="listar-views">
                                                <li class="listar-active"><a href="{{ url('/explore') }}"><i
                                                            class="icon-grid"></i></a></li>
                                            </ul>
                                        </div>
                                    </fieldset>

                                </form>
                            </div>
                        </div>
                        <div class="listar-themeposts listar-placesposts listar-gridview">
                            @if (Auth::check())
                            @foreach ($destinasi as $item)
                            <div class="listar-themepost listar-placespost">
                                <figure class="listar-featuredimg"><a href="{{ url('/detail_destinasi') .'/'.$item->id}}">{{ $item->nama }}"><img
                                            src="{{asset('laravel/public/download/'.$item->foto)}}"
                                            alt="image description" class="mCS_img_loaded"></a></figure>
                                <div class="listar-postcontent">
                                    <h3><a href="{{ url('/detail_destinasi') .'/'.$item->id}}">{{ $item->nama }}</a>
                                    </h3>
                                    <div class="listar-description">
                                        <p>{{ $item->deskripsi }}</p>
                                    </div>
                                    {{-- <div class="listar-reviewcategory">
                                        <div class="listar-review">
                                            <span class="listar-stars"><span></span></span>
                                            <em>(3 Review)</em>
                                        </div>
                                    </div> --}}
                                    <div class="listar-themepostfoot">
                                        <a class="listar-location" target="iframe_a" href="{{ $item->maps }}">
                                            <i class="icon-icons74"></i>
                                            <em>{{ $item->lokasi }}</em>
                                        </a>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="alert alert-success">
                                Silahkan Login Terlebih Dahulu
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--************************************
				Main End
		*************************************-->
@endsection
<!--************************************
			Wrapper End
	*************************************-->
<!--************************************
			Theme Modal Box Start
	*************************************-->

<!--************************************
			Theme Modal Box End
	*************************************-->
<!--************************************
			Login Singup Start
	*************************************-->
