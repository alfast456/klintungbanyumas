@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')

		<!--************************************
				Inner Search Start
		*************************************-->

		<!--************************************
				Inner Search End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="listar-main" class="listar-main listar-haslayout">
			<div class="listar"> <img src="{{ asset('/laravel/public/image/slider/img-01.jpg') }}" alt="" ></div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="listar-content" class="listar-content">
							<div class="listar-contactusarea">
								<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 pull-left">
									<div class="row">
										<form action="{{ url('/contact')}}" method="POST" class="listar-formtheme listar-formcontactus">
                                            @csrf
											<fieldset>
												<h2>Contact Form</h2>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="form-group">
															<input type="text" name="nama" class="form-control" placeholder="Your Name">
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="form-group">
															<input type="email" name="email" class="form-control" placeholder="Email Address">
														</div>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="form-group">
															<textarea class="form-control" name="pesan" placeholder="Message"></textarea>
														</div>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<button class="listar-btn listar-btngreen" type="submit">Send Message</button>
													</div>
												</div>
											</fieldset>
										</form>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 pull-right">
									<div class="row">
										<div class="listar-contactinfo">
											<h2>Klintung</h2>
											<h2>Banyumas</h2>
											<div class="listar-description">
												<p>Temukan tempat terbaik bagikalian yang kurang kasih sayang.</p>
											</div>
											<ul class="listar-contactinfolist">
												<li>
													<i class="icon-"><img src="{{ asset('/laravel/public/image/icons/icon-03.png') }}" alt="image description"></i>
													<span>+628 7754 6884 445</span>
												</li>
												<li>
													<i class="icon-icons208"></i>
													<span><a href="mailto:listingstar@gmail.com">klintungbanyumas@gmail.com</a></span>
												</li>
												<li>
													<i class="icon-world"></i>
													<span><a href="www.listingstar.com" target="_blank">www.klintungbanyumas.com</a></span>
												</li>
												<li>
													<i class="icon-icons74"></i>
													<span>Purwokerto, Jawa Tengah</span>
												</li>
											</ul>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!--************************************
				Main End
		*************************************-->
	<!--************************************
			Login Singup Start
	*************************************-->

@endsection
