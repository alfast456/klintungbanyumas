@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')
@php
use App\Riview;
$repiew=Riview::all();
$riviews=Riview::count();
@endphp
<!--************************************
				Inner Search Start
		*************************************-->
<!--************************************
				Inner Search End
		*************************************-->
        <div class="listar-homebannerslider">
            <br><br><br><br><br><br><br>
        </div>
<!--************************************
				Main Start
		*************************************-->
<main id="listar-main" class="listar-main listar-haslayout">
    <div class="listar-themepost listar-placespost listar-detail listar-detailvone">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="listar-postcontent">
                        <figure class="listar-featuredimg"><center><img src="{{asset('laravel/public/download/'.$destinasi->foto)}}" style="width: 1000px; height: 350px;" alt="image description"></center></figure>
                        <h1>{{ $destinasi->nama }}<i class="icon-checkmark listar-postverified listar-themetooltip"
                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Verified"></i>
                        </h1>
                        <div class="listar-reviewcategory">
                            <div class="listar-review">

                            </div>
                            <ul class="listar-postinfotags">

                                <a id="listar-btnsignin" class="listar-btn listar-btnblue" href="{{ url('/booking') }}">
                                    <span>Booking</span>
                                </a>


                            </ul>
                        </div>

                        <div class="listar-themepostfoot">
                            <ul>
                                <li>
                                    <i class="icon-telephone114"></i>
                                    <span>{{ $destinasi->nohp_wisata }}</span>
                                </li>
                                <li>
                                    <i class="icon-icons74"></i>
                                    <span>{{ $destinasi->lokasi }}</span>
                                </li>
                                <li>
                                    <i class="icon-icons20"></i>
                                    <span>Buka {{ $destinasi->jam }} <span>Tutup</span> {{ $destinasi->tutup }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="listar-themetabs">
                        <ul class="listar-themetabnav" role="tablist">
                            <li role="presentation" class="active"><a href="#overview" aria-controls="overview"
                                    role="tab" data-toggle="tab">Overview</a></li>
                            <li role="presentation"><a href="#location" aria-controls="location" role="tab"
                                    data-toggle="tab">Location</a></li>
                            <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab"
                                    data-toggle="tab">Reviews</a></li>
                            <li role="presentation"><a href="#gallery" aria-controls="gallery" role="tab"
                                    data-toggle="tab">Gallery</a></li>
                        </ul>
                        <div class="tab-content listar-themetabcontent">
                            <div role="tabpanel" class="tab-pane active listar-overview" id="overview">
                                <div class="listar-leftbox">
                                    <p>{{ $destinasi->deskripsi }}</p>
                                    <div class="listar-videobox">
                                        {{-- <iframe width="560" height="315" src="https://www.youtube.com/embed/5IBjlCx8_JE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
                                        <iframe width="560" height="315" src="{{ $destinasi->video }}"frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="listar-rightbox">
                                    <div class="listar-openinghoursarea">
                                        <div class="listar-title">
                                            <h3>Opening Hours</h3>
                                        </div>
                                        <ul class="listar-openinghours">
                                            <li>
                                                <span>Monday</span>
                                                <span>{{ $destinasi->jam }} - {{ $destinasi->tutup }}</span>
                                            </li>
                                            <li>
                                                <span>Tuesday</span>
                                                <span>{{ $destinasi->jam2 }} - {{ $destinasi->tutup }}</span>
                                            </li>
                                            <li>
                                                <span>Wednesday</span>
                                                <span>{{ $destinasi->jam3 }} - {{ $destinasi->tutup }}</span>
                                            </li>
                                            <li>
                                                <span>Thursday</span>
                                                <span>{{ $destinasi->jam4 }} - {{ $destinasi->tutup }}</span>
                                            </li>
                                            <li>
                                                <span>Friday</span>
                                                <span>{{ $destinasi->jam5 }} - {{ $destinasi->tutup }}</span>
                                            </li>
                                            <li>
                                                <span>Saturday</span>
                                                <span>{{ $destinasi->jam6 }} - {{ $destinasi->tutup }}</span>
                                            </li>
                                            <li>
                                                <span>Sunday</span>
                                                <span>{{ $destinasi->jam7 }} - {{ $destinasi->tutup }}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane listar-addressmaplocation" id="location">
                                <iframe src="{{ $destinasi->maps }}" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="reviews">
                                <ul id="listar-comments" class="listar-comments">
                                    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
                                    @foreach ($repiew as $pp)


                                    <li>
                                        <div class="listar-comment">
                                            <div class="listar-commentauthorbox">
                                                <figure><img style="width: 100px" src="{{asset('laravel/public/download/'.$pp->gambar)}}"
                                                            alt="image description"></figure>
                                                <div class="listar-authorinfo">
                                                    <em>{{ $pp->destinasi }}</em>
                                                    @for ($i = 0; $i < $pp->bintang; $i++)
                                                    <i style="color: gold" class="fas fa-star"></i>
                                                    @endfor

                                                </div>
                                            </div>
                                            <div class="listar-commentcontent">
                                                <time datetime="2017-09-09">
                                                    <i class="icon-alarmclock"></i>
                                                    <span>{{ $pp->created_at }}</span>
                                                </time>
                                                <div class="listar-description">
                                                    <p>{{ $pp->komentar }}</p>
                                                    <ul class="listar-authorgallery">
                                                        <li>
                                                            <figure><img
                                                                        src="{{asset('laravel/public/download/'.$pp->gambar)}}"
                                                                        alt="image description"
                                                                        style="width: 200px"></figure>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach

                                </ul>
                                <nav class="listar-pagination">
                                    <ul>
                                        <li class="listar-prevpage"><a href="javascript:void(0);"><i
                                                    class="fa fa-angle-left"></i></a></li>
                                        <li><a href="javascript:void(0);">1</a></li>
                                        <li class="listar-nextpage"><a href="javascript:void(0);"><i
                                                    class="fa fa-angle-right"></i></a></li>
                                    </ul>
                                </nav>
                                <div class="listar-formreviewarea">
                                    <h3>Add Review</h3>
                                    <form action="{{ url('/detail_destinasi') .'/'.$destinasi->id}}" method="POST" enctype="multipart/form-data"
                                        class="listar-formtheme listar-formaddreview">
                                        @csrf
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <p>Tempat Destinasi</p>
                                                        <input type="text" name="destinasi" value="{{ $destinasi->nama}}">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <p>Your rating for this listing</p>
                                                        <select name="bintang">
                                                            <option>Silahkan pilih rating</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>

                                                    </div>
                                                </div>


                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <label class="listar-fileuploadlabel" for="listar-photogallery">
                                                        <i class="icon-upload3"></i>
                                                        <span>Upload Images</span>
                                                        <input class="form-group"
                                                            type="file" name="gambar">
                                                    </label>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="form-group">
                                                        <textarea name="komentar" class="form-control"
                                                            placeholder="Review"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <button class="listar-btn listar-btngreen" type="submit">Submit
                                                        Review</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="gallery">
                                <div id="listar-postgallery" class="listar-postgallery">
                                    <div class="listar-masnory">
                                        <figure><a href="{{asset('laravel/public/download/'.$destinasi->foto)}}" data-rel="prettyPhoto[gallery]"><img
                                                    src="{{asset('laravel/public/download/'.$destinasi->foto)}}" alt="image description"></a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--************************************
				Main End
		*************************************-->

<!--************************************
			Login Singup Start
	*************************************-->

<!--************************************
			Login Singup End
	*************************************-->
@endsection

<!--************************************
			Theme Modal Box Start
	*************************************-->
