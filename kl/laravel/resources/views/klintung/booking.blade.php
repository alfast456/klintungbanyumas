@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')
@php
    use App\Destinasi;
    $destinasi=destinasi::all();
@endphp
        <main id="listar-main" class="listar-main listar-haslayout">
			<div class="listar"> <p></p><br><p></p><br><p></p><br><p></p><br><p></p><br><p></p><br><p></p><br></div>
			<div class="container">
				<div class="row">
					<div >
						<div id="listar-content" class="listar-content">
							<div class="listar-contactusarea">
								<div class="col-xs-12 col-sm-12 ">
									<div class="row">
										<form action="booking" method="POST" class="listar-formtheme listar-formcontactus">
											@csrf
                                            <fieldset>
											@if(session()->has('pesan'))
                    						<div class="alert alert-success">{{ session()->get('pesan') }}
                    						</div>
                    						@endif
												<h2>Booking</h2>
												<div class="row">

													<div >
														<div class="form-group">
															<input type="text" name="email_user" class="form-control" placeholder="email">
														</div>
													</div>
                                                    <div >
														<div class="form-group">
															<input type="text" name="no_hp" class="form-control" placeholder="no_hp">
														</div>
													</div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div >
                                                            <div class="form-group">
                                                                <select name="destinasi" class="form-control">
                                                                    <option>Pilih Destinasi Wisata</option>
                                                                    @foreach ($destinasi as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="form-group">
															<input type="date" class="form-control" id="booking" name="tgl_booking">
														</div>
													</div>
                                                    <div>
														<div class="form-group">
															<input type="number" name="jumlah_tiket" class="form-control" placeholder="Jumlah Tiket">
														</div>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<button class="listar-btn listar-btngreen" type="submit" >Send Message</button>
													</div>
												</div>
											</fieldset>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</main>


@endsection
