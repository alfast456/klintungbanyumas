@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')>
		<!--************************************
				Inner Search Start
		*************************************-->
		<div class="listar-innerpagesearch">
			<a id="listar-btnsearchtoggle" class="listar-btnsearchtoggle" href="javascript:void(0);"><i class="icon-icons185"></i></a>
			<div id="listar-innersearch" class="listar-innersearch">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<form class="listar-formtheme listar-formsearchlisting">
								<fieldset>
									<div class="form-group listar-inputwithicon">
										<i class="icon-layers"></i>
										<div class="listar-select">
											<select id="listar-categorieschosen" class="listar-categorieschosen listar-chosendropdown">
												<option>Ex: Food, Retail, hotel, cinema</option>
												<option class="icon-entertainment">Art &amp; Entertainment</option>
												<option class="icon-shopping">Beauty &amp; Health</option>
												<option class="icon-study">Education</option>
												<option class="icon-healthfitness">Fitness</option>
												<option class="icon-icons240">Hotels</option>
												<option class="icon-localservice">Motor Mechanic</option>
												<option class="icon-nightlife">Night Life</option>
												<option class="icon-tourism">Restaurant</option>
												<option class="icon-shopping">Real Estate</option>
												<option class="icon-shopping">Shopping</option>
											</select>
										</div>
									</div>
									<div class="form-group listar-inputwithicon">
										<i class="icon-global"></i>
										<div class="listar-select listar-selectlocation">
											<select id="listar-locationchosen" class="listar-locationchosen listar-chosendropdown">
												<option>Choose a Location</option>
												<option>Lahore</option>
												<option>Bayonne</option>
												<option>Greenville</option>
												<option>Manhattan</option>
												<option>Queens</option>
												<option>The Heights</option>
											</select>
										</div>
									</div>
									<div class="form-group listar-inputwithicon">
										<i class=""><img src="images/icons/icon-01.png" alt="image description"></i>
										<p>Price: </p>
										<!-- <input id="listar-rangeslider" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14"> -->
										<input id="listar-rangeslider" class="listar-rangeslider" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14">
									</div>
									<button type="button" class="listar-btn listar-btngreen">Search Places</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Search End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="listar-main" class="listar-main listar-haslayout">
			<div id="listar-twocolumns" class="listar-twocolumns">
				<div class="listar-themepost listar-placespost listar-detail listar-detailvtwo">
					<figure class="listar-featuredimg">
						<img src="images/post/img-23.jpg" alt="image description">
						<figcaption>
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="listar-postcontent">
											<div class="listar-reviewcategory">
												<h1>Salt &amp; Pepper Emporium<i class="icon-checkmark listar-postverified listar-themetooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Verified"></i></h1>
												<div class="listar-review">
													<span class="listar-stars"><span></span></span>
													<em>(6 Review)</em>
												</div>
											</div>
											<ul class="listar-postinfotags">
												<li><a href="javascript:void(0);"><i class="icon-heart2"></i><span>23</span></a></li>
												<li>
													<div class="listar-btnquickinfo">
														<a class="listar-btnshare" href="javascript:void(0);">
															<i class="icon-share3"></i>
															<span>share</span>
														</a>
														<div class="listar-btnquickinfo">
															<div class="listar-shareicons">
																<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
															</div>
														</div>
													</div>
												</li>
												<li><span class="listar-tagviews"><i class="icon-eye2"></i><span>52</span></span></li>
											</ul>
											<div class="listar-themepostfoot">
												<ul>
													<li>
														<i class="icon-telephone114"></i>
														<span>+ 7890 456 133</span>
													</li>
													<li>
														<i class="icon-icons74"></i>
														<span>Manhattan Hall, London W1K 2EQ UK</span>
													</li>
													<li>
														<i class="icon-icons20"></i>
														<span>Today <span>Closed Now</span> 10:00 AM - 5:00 PM</span>
													</li>
													<li>
														<i class="icon-global"></i>
														<span><a href="www.listingstar.com">www.listingstar.com</a></span>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</figcaption>
					</figure>
					<div class="clearfix"></div>
					<div class="container">
						<div class="row">
							<div id="listar-detailcontent" class="listar-detailcontent">
								<div class="listar-content">
									<div class="listar-themetabs">
										<div id="listar-fixedtabnav" class="listar-fixedtabnav">
											<div class="container">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<ul class="listar-themetabnav">
															<li><a href="#listar-overview">Overview</a></li>
															<li><a href="#listar-pricing">Pricing</a></li>
															<li><a href="#listar-addressmaplocation">Location</a></li>
															<li><a href="#listar-reviews">Reviews</a></li>
														</ul>
														<ul class="listar-socialicons listar-socialiconsborder">
															<li class="listar-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
															<li class="listar-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
															<li class="listar-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
															<li class="listar-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
															<li class="listar-instagram"><a href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
															<li class="listar-vimeo"><a href="javascript:void(0);"><i class="fa fa-vimeo"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<ul id="listar-themetabnav" class="listar-themetabnav">
											<li class="listar-active"><a href="#listar-overview">Overview</a></li>
											<li><a href="#listar-pricing">Pricing</a></li>
											<li><a href="#listar-addressmaplocation">Location</a></li>
											<li><a href="#listar-reviews">Reviews</a></li>
										</ul>
										<div class="listar-sectionholder">
											<div id="listar-overview" class="listar-overview">
												<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue Sed non mauris vitae;erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nunc commodo nisi id viverra venenatis.</p>
												<p>Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam.</p>
												<div class="listar-videobox">
													<iframe src="https://player.vimeo.com/video/234265016?byline=0&portrait=0"></iframe>
												</div>
												<div class="listar-amenitiesarea">
													<div class="listar-title">
														<h3>Amenities</h3>
													</div>
													<ul class="listar-amenities">
														<li>Pets allowed</li>
														<li>Kitchen</li>
														<li>Internet</li>
														<li>Suitable for events</li>
														<li>Gym</li>
														<li>Dryer</li>
														<li>Hot tub</li>
														<li>Family/kid friendly</li>
														<li>Doorman</li>
														<li>Cable TV</li>
														<li>Wheelchair accessible</li>
														<li>Wireless Internet</li>
														<li>Pool</li>
													</ul>
												</div>
											</div>
											<div id="listar-pricing" class="listar-pricing">
												<div class="listar-title">
													<h3>Pricing</h3>
												</div>
												<ul class="listar-prices">
													<li>
														<div class="listar-pricebox">
															<h3>Chicken Corn Soup</h3>
															<p>Served with Fish Crackers (Individual)</p>
															<span class="listar-price">$12</span>
														</div>
													</li>
													<li>
														<div class="listar-pricebox">
															<h3>Chicken Corn Soup</h3>
															<p>Served with Fish Crackers (Individual)</p>
															<span class="listar-price">$12</span>
														</div>
													</li>
													<li>
														<div class="listar-pricebox">
															<h3>Masala Fries</h3>
															<p>Served with Fish Crackers (Individual)</p>
															<span class="listar-price">$5</span>
														</div>
													</li>
													<li>
														<div class="listar-pricebox">
															<h3>Chicken Corn Soup</h3>
															<p>Served with Fish Crackers (Individual)</p>
															<span class="listar-price">$12</span>
														</div>
													</li>
													<li>
														<div class="listar-pricebox">
															<h3>Inside Out Sandwich - On Its Own</h3>
															<p>Served with Fish Crackers (Individual)</p>
															<span class="listar-price">$54</span>
														</div>
													</li>
												</ul>
											</div>
											<div id="listar-addressmaplocation" class="listar-addressmaplocation">
												<div class="listar-title">
													<h3>Location</h3>
												</div>
												<div id="listar-locationmap" class="listar-locationmap"></div>
											</div>
											<div id="listar-reviews" class="listar-reviews">
												<div class="listar-title">
													<h3>Reviews</h3>
												</div>
												<ul id="listar-comments" class="listar-comments">
													<li>
														<div class="listar-comment">
															<div class="listar-commentauthorbox">
																<figure><a href="javascript:void(0);"><img src="images/img-02.jpg" alt="image description"></a></figure>
																<div class="listar-authorinfo">
																	<h3>Katie</h3>
																	<em>Family Vacation</em>
																	<span class="listar-stars"><span></span></span>
																</div>
															</div>
															<div class="listar-commentcontent">
																<time datetime="2017-09-09">
																	<i class="icon-alarmclock"></i>
																	<span>January 25, 2017</span>
																</time>
																<div class="listar-description">
																	<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue Sed non mauris vitae;erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
																	<ul class="listar-authorgallery">
																		<li><figure><a href="images/img-03.jpg" data-rel="prettyPhoto[userimgone]"><img src="images/img-03.jpg" alt="image description"></a></figure></li>
																		<li><figure><a href="images/img-04.jpg" data-rel="prettyPhoto[userimgone]"><img src="images/img-04.jpg" alt="image description"></a></figure></li>
																	</ul>
																</div>
																<a class="listar-helpful" href="javascript:void(0);">
																	<i class="icon-thumb-up2"></i>
																	<span>Helpful</span>
																	<span>1</span>
																</a>
															</div>
														</div>
													</li>
													<li>
														<div class="listar-comment">
															<div class="listar-commentauthorbox">
																<figure><a href="javascript:void(0);"><img src="images/img-02.jpg" alt="image description"></a></figure>
																<div class="listar-authorinfo">
																	<h3>Katie</h3>
																	<em>Family Vacation</em>
																	<span class="listar-stars"><span></span></span>
																</div>
															</div>
															<div class="listar-commentcontent">
																<time datetime="2017-09-09">
																	<i class="icon-alarmclock"></i>
																	<span>January 25, 2017</span>
																</time>
																<div class="listar-description">
																	<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue Sed non mauris vitae;erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra justo.</p>
																	<p>First, please don’t fall sick. However, if in case something does catchup with you, we will airlift you to hospital but your insurance will have to pay for this. Ulins aliquam massa nisl quis neque. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut liquam massa nisl quis neque.</p>
																</div>
																<a class="listar-helpful" href="javascript:void(0);">
																	<i class="icon-thumb-up2"></i>
																	<span>Helpful</span>
																</a>
															</div>
														</div>
													</li>
													<li>
														<div class="listar-comment">
															<div class="listar-commentauthorbox">
																<figure><a href="javascript:void(0);"><img src="images/img-02.jpg" alt="image description"></a></figure>
																<div class="listar-authorinfo">
																	<h3>Katie</h3>
																	<em>Family Vacation</em>
																	<span class="listar-stars"><span></span></span>
																</div>
															</div>
															<div class="listar-commentcontent">
																<time datetime="2017-09-09">
																	<i class="icon-alarmclock"></i>
																	<span>January 25, 2017</span>
																</time>
																<div class="listar-description">
																	<p>What a magical place, even better than I imagined! Teresa and Daniella were so helpful and awesome</p>
																</div>
																<a class="listar-helpful" href="javascript:void(0);">
																	<i class="icon-thumb-up2"></i>
																	<span>Helpful</span>
																</a>
															</div>
														</div>
													</li>
													<li>
														<div class="listar-comment">
															<div class="listar-commentauthorbox">
																<figure><a href="javascript:void(0);"><img src="images/img-02.jpg" alt="image description"></a></figure>
																<div class="listar-authorinfo">
																	<h3>Katie</h3>
																	<em>Family Vacation</em>
																	<span class="listar-stars"><span></span></span>
																</div>
															</div>
															<div class="listar-commentcontent">
																<time datetime="2017-09-09">
																	<i class="icon-alarmclock"></i>
																	<span>January 25, 2017</span>
																</time>
																<div class="listar-description">
																	<p>Very nice place</p>
																	<ul class="listar-authorgallery">
																			<li><figure><a href="images/img-04.jpg" data-rel="prettyPhoto[userimgtwo]"><img src="images/img-04.jpg" alt="image description"></a></figure></li>
																	</ul>
																</div>
																<a class="listar-helpful" href="javascript:void(0);">
																	<i class="icon-thumb-up2"></i>
																	<span>Helpful</span>
																	<span>1</span>
																</a>
															</div>
														</div>
													</li>
													<li>
														<div class="listar-comment">
															<div class="listar-commentauthorbox">
																<figure><a href="javascript:void(0);"><img src="images/img-02.jpg" alt="image description"></a></figure>
																<div class="listar-authorinfo">
																	<h3>Katie</h3>
																	<em>Family Vacation</em>
																	<span class="listar-stars"><span></span></span>
																</div>
															</div>
															<div class="listar-commentcontent">
																<time datetime="2017-09-09">
																	<i class="icon-alarmclock"></i>
																	<span>January 25, 2017</span>
																</time>
																<div class="listar-description">
																	<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue Sed non mauris vitae;erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo First, please don’t fall sick. However, if in case something does catchup.</p>
																</div>
																<a class="listar-helpful" href="javascript:void(0);">
																	<i class="icon-thumb-up2"></i>
																	<span>Helpful</span>
																	<span>5</span>
																</a>
															</div>
														</div>
													</li>
												</ul>
												<nav class="listar-pagination">
													<ul>
														<li class="listar-prevpage"><a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a></li>
														<li><a href="javascript:void(0);">1</a></li>
														<li><a href="javascript:void(0);">2</a></li>
														<li><a href="javascript:void(0);">3</a></li>
														<li class="listar-nextpage"><a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a></li>
													</ul>
												</nav>
												<div class="listar-formreviewarea">
													<h3>Add Review</h3>
													<form class="listar-formtheme listar-formaddreview">
														<fieldset>
															<div class="row">
																<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																	<div class="listar-rating">
																		<p>Your rating for this listing</p>
																		<span class="listar-stars"></span>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																	<label class="listar-fileuploadlabel" for="listar-photogallery">
																		<i class="icon-upload3"></i>
																		<span>Upload Images</span>
																		<input id="listar-photogallery" class="listar-fileinput" type="file" name="file">
																	</label>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																	<div class="form-group">
																		<input type="text" name="yourname" class="form-control" placeholder="Your Name">
																	</div>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																	<div class="form-group">
																		<input type="text" name="emailaddress" class="form-control" placeholder="Email Address">
																	</div>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																	<div class="form-group">
																		<span class="listar-select">
																			<select>
																				<option>Family Vacation</option>
																				<option>Family Vacation</option>
																				<option>Family Vacation</option>
																			</select>
																		</span>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																	<div class="form-group">
																		<textarea name="review" class="form-control" placeholder="Review"></textarea>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																	<button class="listar-btn listar-btngreen" type="button">Submit Review</button>
																</div>
															</div>
														</fieldset>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								<aside id="listar-stickysidebar" class="listar-sidebar listar-stickysidebar">
									<div class="sidebar__inner">
										<div class="listar-widget listar-widgetsocialicon">
											<div class="listar-widgetcontent">
												<ul class="listar-socialicons listar-socialiconsborder">
													<li class="listar-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
													<li class="listar-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
													<li class="listar-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
													<li class="listar-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
													<li class="listar-instagram"><a href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
													<li class="listar-vimeo"><a href="javascript:void(0);"><i class="fa fa-vimeo"></i></a></li>
												</ul>
											</div>
										</div>
										<div class="listar-widget listar-widgetopeninghours">
											<div class="listar-widgettitle">
												<h3>
													<i class="icon-alarmclock"></i>
													<span>Opening Hours</span>
												</h3>
											</div>
											<div class="listar-widgetcontent">
												<ul class="listar-openinghours">
													<li>
														<span>Monday</span>
														<span>10:00 AM - 5:00 PM</span>
													</li>
													<li>
														<span>Tuesday</span>
														<span>10:00 AM - 5:00 PM</span>
													</li>
													<li>
														<span>Wednesday</span>
														<span>10:00 AM - 5:00 PM</span>
													</li>
													<li>
														<span>Thursday</span>
														<span>10:00 AM - 5:00 PM</span>
													</li>
													<li>
														<span>Friday</span>
														<span>10:00 AM - 5:00 PM</span>
													</li>
													<li>
														<span>Saturday</span>
														<span>10:00 AM - 3:00 PM</span>
													</li>
													<li>
														<span>Sunday</span>
														<span>Closed</span>
													</li>
												</ul>
											</div>
										</div>
										<div class="listar-widget">
											<div class="listar-widgettitle">
												<h3>
													<i class="icon-trophy"></i>
													<span>Price</span>
												</h3>
											</div>
											<div class="listar-widgetcontent">
												<div class="listar-range">
													<i class="listar-themetooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Moderate"><span class="listar-dollars"><span></span></span></i>
													<span>Price Ranger: <strong>$10 - $200</strong></span>
												</div>
												<a class="listar-btnclainnow" href="javascript:void(0);">Claim Now</a>
											</div>
										</div>
										<div class="listar-widget">
											<div class="listar-widgettitle">
												<h3>
													<i class="icon-hotairballoon"></i>
													<span>Additional Details</span>
												</h3>
											</div>
											<div class="listar-widgetcontent">
												<ul class="listar-additionaldetails">
													<li>
														<span>Take Out:</span>
														<span>Yes</span>
													</li>
													<li>
														<span>Delivery</span>
														<span>No</span>
													</li>
													<li>
														<span>Caters</span>
														<span>Yes</span>
													</li>
													<li>
														<span>Smoking</span>
														<span>No</span>
													</li>
													<li>
														<span>Take Out</span>
														<span>Yes</span>
													</li>
													<li>
														<span>Delivery</span>
														<span>No</span>
													</li>
													<li>
														<span>Caters</span>
														<span>Yes</span>
													</li>
													<li>
														<span>Smoking</span>
														<span>Yes</span>
													</li>
												</ul>
											</div>
										</div>
										<div class="listar-widget listar-widgetformauthor">
											<div class="listar-widgetcontent">
												<div class="listar-authorinfo">
													<figure><a href="javascript:void(0);"><img src="images/img-05.jpg" alt="image description"></a></figure>
													<div class="listar-authorcontent">
														<h4>Sara Loi</h4>
														<ul class="listar-socialicons listar-socialiconsborder">
															<li class="listar-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
															<li class="listar-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
															<li class="listar-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
															<li class="listar-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
														</ul>
													</div>
												</div>
												<form class="listar-formtheme listar-formauthor">
													<fieldset>
														<div class="form-group">
															<input type="text" name="yourname" class="form-control" placeholder="Your Name">
														</div>
														<div class="form-group">
															<input type="text" name="yourname" class="form-control" placeholder="Your Name">
														</div>
														<div class="form-group">
															<input type="text" name="yourname" class="form-control" placeholder="Your Name">
														</div>
														<div class="form-group">
															<textarea name="yourname" class="form-control" placeholder="Your Name"></textarea>
														</div>
														<button type="button" class="listar-btn listar-btngreen">Send</button>
													</fieldset>
												</form>
											</div>
										</div>
									</div>
								</aside>
							</div>
						</div>
					</div>
				</div>
				<section class="listar-sectionspace listar-bglight listar-listingvtwo listar-haslayout">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="listar-title">
									<h3>Amenities</h3>
								</div>
								<div id="listar-relatedlistingslider" class="listar-relatedlistingslider listar-gridview owl-carousel">
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-13.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Tourist Guide</a></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-nightlife"></i>
															<span>Night Life</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-14.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Serena Hotel</a><i class="icon-checkmark listar-postverified listar-themetooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Verified"></i></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-tourism"></i>
															<span>Hotel</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo listar-liked" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-15.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Tourist Guide</a></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-foods"></i>
															<span>Food and Drinks</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-13.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Tourist Guide</a></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-nightlife"></i>
															<span>Night Life</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-14.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Serena Hotel</a><i class="icon-checkmark listar-postverified listar-themetooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Verified"></i></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-tourism"></i>
															<span>Hotel</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo listar-liked" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-15.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Tourist Guide</a></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-foods"></i>
															<span>Food and Drinks</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-13.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Tourist Guide</a></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-nightlife"></i>
															<span>Night Life</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-14.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Serena Hotel</a><i class="icon-checkmark listar-postverified listar-themetooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Verified"></i></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-tourism"></i>
															<span>Hotel</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo listar-liked" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
									<div class="item">
										<div class="listar-themepost listar-placespost">
											<figure class="listar-featuredimg">
												<img src="images/post/img-15.jpg" alt="image description">
												<div class="listar-postcontent">
													<h3><a href="javascript:void(0);">Tourist Guide</a></h3>
													<div class="listar-reviewcategory">
														<div class="listar-review">
															<span class="listar-stars"><span></span></span>
															<em>(3 Review)</em>
														</div>
														<a href="javascript:void(0);" class="listar-category">
															<i class="icon-foods"></i>
															<span>Food and Drinks</span>
														</a>
													</div>
													<div class="listar-themepostfoot">
														<a class="listar-location" href="javascript:void(0);">
															<i class="icon-icons74"></i>
															<em>New York</em>
														</a>
														<div class="listar-postbtns">
															<a class="listar-btnquickinfo" href="#" data-toggle="modal" data-target=".listar-placequickview"><i class="icon-expand"></i></a>
															<a class="listar-btnquickinfo" href="javascript:void(0);"><i class="icon-heart2"></i></a>
															<div class="listar-btnquickinfo">
																<div class="listar-shareicons">
																	<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
																	<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
																</div>
																<a class="listar-btnshare" href="javascript:void(0);">
																	<i class="icon-share3"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</figure>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</main>
		<!--************************************
				Main End
		*************************************-->
	@endsection
	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Login Singup Start
	*************************************-->
	<div id="listar-loginsingup" class="listar-loginsingup">
		<button type="button" class="listar-btnclose">x</button>
		<figure class="listar-loginsingupimg" data-vide-bg="poster: images/bgjoin.jpg" data-vide-options="position: 50% 50%"></figure>
		<div class="listar-contentarea">
			<div class="listar-themescrollbar">
				<div class="listar-logincontent">
					<div class="listar-themetabs">
						<ul class="listar-tabnavloginregistered" role="tablist">
							<li role="presentation" class="active"><a href="#listar-loging" data-toggle="tab">Log in</a></li>
							<li role="presentation"><a href="#listar-register" data-toggle="tab">Register</a></li>
						</ul>
						<div class="tab-content listar-tabcontentloginregistered">
							<div role="tabpanel" class="tab-pane active fade in" id="listar-loging">
								<form class="listar-formtheme listar-formlogin">
									<fieldset>
										<div class="form-group listar-inputwithicon">
											<i class="icon-profile-male"></i>
											<input type="text" name="username" class="form-control" placeholder="Username Or Email">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-icons208"></i>
											<input type="password" name="password" class="form-control" placeholder="Password">
										</div>
										<div class="form-group">
											<div class="listar-checkbox">
												<input type="checkbox" name="remember" id="rememberpass2">
												<label for="rememberpass2">Remember me</label>
											</div>
											<span><a href="#">Lost your Password?</a></span>
										</div>
										<button class="listar-btn listar-btngreen">Register</button>
									</fieldset>
								</form>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="listar-register">
								<form class="listar-formtheme listar-formlogin">
									<fieldset>
										<div class="form-group listar-inputwithicon">
											<i class="icon-profile-male"></i>
											<input type="text" name="username" class="form-control" placeholder="Username">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-icons208"></i>
											<input type="email" name="emailaddress" class="form-control" placeholder="Email Address">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-lock-stripes"></i>
											<input type="password" name="password" class="form-control" placeholder="Password">
										</div>
										<div class="form-group listar-inputwithicon">
											<i class="icon-lock-stripes"></i>
											<input type="password" name="confirmpassword" class="form-control" placeholder="Password">
										</div>
										<div class="form-group">
											<div class="listar-checkbox">
												<input type="checkbox" name="remember" id="rememberpass">
												<label for="rememberpass">Remember me</label>
											</div>
											<span><a href="#">Lost your Password?</a></span>
										</div>
										<button class="listar-btn listar-btngreen">login</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="listar-shareor"><span>or</span></div>
					<div class="listar-signupwith">
						<h2>Sign in With...</h2>
						<ul class="listar-signinloginwithsocialaccount">
							<li class="listar-facebook"><a href="javascript:void(0);"><i class="icon-facebook-1"></i><span>Facebook</span></a></li>
							<li class="listar-twitter"><a href="javascript:void(0);"><i class="icon-twitter-1"></i><span>Twitter</span></a></li>
							<li class="listar-googleplus"><a href="javascript:void(0);"><i class="icon-google4"></i><span>Google +</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--************************************
			Login Singup End
	*************************************-->
	<!--************************************
			Theme Modal Box Start
	*************************************-->
	<div class="modal fade listar-placequickview" tabindex="-1" role="dialog">
		<div class="modal-dialog listar-modaldialog" role="document">
			<div class="modal-content listar-modalcontent">
				<div class="listar-themepost listar-placespost">
					<span class="listar-btnclosequickview" data-toggle="modal" data-target=".listar-placequickview">X</span>
					<figure class="listar-featuredimg" data-vide-bg="poster: images/post/img-16.jpg" data-vide-options="position: 50% 50%">
						<span class="listar-contactnumber">
							<i class="icon-"><img src="images/icons/icon-03.png" alt="image description"></i>
							<em> + 7890 456 133</em>
						</span>
					</figure>
					<div class="listar-postcontent">
						<h3><a href="javascript:void(0);">Serena Hotel</a><i class="icon-checkmark listar-postverified listar-themetooltip" data-toggle="tooltip" data-placement="top" title="Verified"></i></h3>
						<div class="listar-description">
							<p>Ut euismod ultricies sollicitudin. Curabitur sed dapibus nulla. Nulla eget iaculis lectus. Mauris ac maximus neque. Nam in mauris quis libero sodales eleifend. Morbi varius, nulla sit Nam in mauris quis libero sodales eleifend amet rutrum elementum, est elit finibus tellus, ut tristique elit risus at metus</p>
						</div>
						<ul class="listar-listfeatures">
							<li>Pets allowed</li>
							<li>Kitchen</li>
							<li>Internet</li>
							<li>Suitable for events</li>
							<li>Gym</li>
							<li>Dryer</li>
							<li>Hot tub</li>
							<li>Family/kid friendly</li>
							<li>Wireless Internet</li>
						</ul>
						<div class="listar-reviewcategory">
							<div class="listar-review">
								<span class="listar-stars"><span></span></span>
								<em>(3 Review)</em>
							</div>
							<a href="javascript:void(0);" class="listar-category">
								<i class="icon-tourism"></i>
								<span>Hotel</span>
							</a>
						</div>
						<div class="listar-themepostfoot">
							<span class="listar-openinghours">
								<i class="icon-alarmclock2"></i>
								<em>Today <span class="listar-greenthemecolor">Open Now</span> 10:00 AM - 5:00 PM</em>
							</span>
							<div class="listar-postbtns">
								<a class="listar-btnquickinfo listar-liked" href="javascript:void(0);"><i class="icon-heart2"></i></a>
								<div class="listar-btnquickinfo">
									<div class="listar-shareicons">
										<a href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
										<a href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
										<a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a>
									</div>
									<a class="listar-btnshare" href="javascript:void(0);">
										<i class="icon-share3"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>