@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')
		<!--************************************
				Inner Search Start
		*************************************-->
		<div class="listar-innerpagesearch">
			<a id="listar-btnsearchtoggle" class="listar-btnsearchtoggle" href="javascript:void(0);"><i class="icon-icons185"></i></a>
			<div id="listar-innersearch" class="listar-innersearch">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<form class="listar-formtheme listar-formsearchlisting">
								<fieldset>
									<div class="form-group listar-inputwithicon">
										<i class="icon-layers"></i>
										<div class="listar-select">
											<select id="listar-categorieschosen" class="listar-categorieschosen listar-chosendropdown">
												<option>Ex: Food, Retail, hotel, cinema</option>
												<option class="icon-entertainment">Art &amp; Entertainment</option>
												<option class="icon-shopping">Beauty &amp; Health</option>
												<option class="icon-study">Education</option>
												<option class="icon-healthfitness">Fitness</option>
												<option class="icon-icons240">Hotels</option>
												<option class="icon-localservice">Motor Mechanic</option>
												<option class="icon-nightlife">Night Life</option>
												<option class="icon-tourism">Restaurant</option>
												<option class="icon-shopping">Real Estate</option>
												<option class="icon-shopping">Shopping</option>
											</select>
										</div>
									</div>
									<div class="form-group listar-inputwithicon">
										<i class="icon-global"></i>
										<div class="listar-select listar-selectlocation">
											<select id="listar-locationchosen" class="listar-locationchosen listar-chosendropdown">
												<option>Choose a Location</option>
												<option>Lahore</option>
												<option>Bayonne</option>
												<option>Greenville</option>
												<option>Manhattan</option>
												<option>Queens</option>
												<option>The Heights</option>
											</select>
										</div>
									</div>
									<div class="form-group listar-inputwithicon">
										<i class=""><img src="images/icons/icon-01.png" alt="image description"></i>
										<p>Price: </p>
										<!-- <input id="listar-rangeslider" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14"> -->
										<input id="listar-rangeslider" class="listar-rangeslider" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14">
									</div>
									<button type="button" class="listar-btn listar-btngreen">Search Places</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Search End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="listar-main" class="listar-main listar-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="listar-content" class="listar-content">
							<div class="listar-404error">
								<h2>404<span>Page not Found</span></h2>
								<h3>Sorry but the page that you are looking for does not exist</h3>
								<a class="listar-btn listar-btngreen" href="javascript:void(0);">Go Back to Home</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!--************************************
				Main End
		*************************************-->
@endsection