@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')
@php
use App\Riview;
$repiew=Riview::all();
$riviews=Riview::count();
@endphp
		<!--************************************
				Home Slider Start
		*************************************-->
		<div class="listar-homebannerslider">
			<div id="listar-homeslider" class="listar-homeslider owl-carousel">
				<div class="item"><figure><img src="{{ asset('/laravel/public/image/slider/img-01.jpg') }}" alt="image description"></figure></div>
				<div class="item"><figure><img src="{{ asset('/laravel/public/image/slider/img-02.jpg') }}" alt="image description"></figure></div>
			</div>
			<div class="listar-homebanner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="listar-bannercontent">
								<h1>Explore Banyumas Lurr</h1>
								<div class="listar-description">
									<p>Temukan tempat terbaik bagi kalian yang kurang kasih sayang</p>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Home Slider End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="listar-main" class="listar-main listar-haslayout">
			<section class="listar-sectionspace listar-bglight listar-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="listar-sectionhead">
								<div class="listar-sectiontitle">
									<h2>Temukan Tempat yang Kamu Inginkan</h2>
								</div>
								<div class="listar-description">
									<p>Eksplore tempat wisata dan pendatang luar kota yang ingin mencari tempat berlibur di Banyumas.</p>
								</div>
							</div>
							<div class="listar-horizontalthemescrollbar">
                                @foreach ($destinasis as $wisata)
								<div class="listar-themepost listar-placespost">
									<figure class="listar-featuredimg"><a href="{{ url('/detail_destinasi') .'/'.$wisata->id}}">{{ $wisata->nama }}"><img style="width: 401px; height: 299px;" src="{{asset('laravel/public/download/'.$wisata->foto)}}" alt="image description"></a></figure>
									<div class="listar-postcontent">
										<h3><a href="{{ url('/') }}/detail_destinasi/{{ $wisata->id }}">{{$wisata->nama}}</a></h3>
										<div class="listar-description">
											<p>{{$wisata->deskripsi}}</p>
										</div>
										<div class="listar-themepostfoot">
											<a class="listar-location" href="javascript:void(0);">
												<i class="icon-icons74"></i>
												<em>{{$wisata->lokasi}}</em>
											</a>

										</div>
									</div>
								</div>
                                @endforeach
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Discover New Places End
			*************************************-->
			<!--************************************
					Add Listing Start
			*************************************-->

			<!--************************************
					Add Listing End
			*************************************-->
			<!--************************************
					Blog Post Start
			*************************************-->

		</main>
		<!--************************************
				Main End
		*************************************-->

	<!--************************************
			Login Singup End
	*************************************-->
	@endsection

