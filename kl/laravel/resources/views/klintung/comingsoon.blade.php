@extends('layouts.app')
@section('header')
@include('layouts.header')
@endsection
@section('content')>
		<!--************************************
				Main Start
		*************************************-->
		<main id="listar-main" class="listar-main listar-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
						<div id="listar-content" class="listar-content">
							<div class="listar-comingsooncontent">
								<h1>Coming Soon</h1>
								<div id="listar-comingsooncounter" class="listar-comingsooncounter"></div>
								<div class="listar-formarea">
									<h2>Please subscribe to newsletter to get updates from us.</h2>
									<form class="listar-formtheme listar-formnewsletter">
										<fieldset>
											<input type="email" name="email" class="form-control" placeholder="Your email address">
											<button class="listar-btn listar-btngreen" type="button">Submit</button>
										</fieldset>
									</form>
								</div>
								<ul class="listar-socialicons listar-socialiconsborder">
									<li class="listar-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
									<li class="listar-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
									<li class="listar-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
									<li class="listar-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
									<li class="listar-instagram"><a href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!--************************************
				Main End
		*************************************-->
	</div>
@endsection